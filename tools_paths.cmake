######################################################################
# This file contains paths for different tools, used by build system
######################################################################

SET (LU4_INSTALLATION_PREFIX "${ROOT_DIR}/sandbox" CACHE FILEPATH "Intermediate installation directory")