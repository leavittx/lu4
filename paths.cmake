######################################
# File paths
######################################
SET (ROOT_DIR "${CMAKE_SOURCE_DIR}/")
SET (SRC_ROOT_DIR "${CMAKE_SOURCE_DIR}/src")
SET (GLOBAL_INCLUDE_DIR "${CMAKE_SOURCE_DIR}/include")
SET (LIB_DIR "${CMAKE_SOURCE_DIR}/lib")

MESSAGE(${ROOT_DIR})


#####################################################
# This paths should be changed in the local copy
#####################################################
SET (QT_PATH "C:/Qt/Qt5.1.1/5.1.1/msvc2012_64_opengl/" CACHE FILEPATH "Qt SDK")
SET (WIN_OPENGL_LIB_PATH "C:/Program Files (x86)/Windows Kits/8.0/Lib/win8/um/x64/" CACHE FILEPATH "Windows OpenGL drivers")

#####################################################
# Setup auxiliary paths for Qt
#####################################################
SET (QT_BINARY_DIR "${QT_PATH}/bin" CACHE FILEPATH "Qt SDK binary directory")


#####################################################
# Setup paths, used by CMake to find OpenGL drivers 
# and Qt5.1 location
#####################################################
LIST(APPEND CMAKE_PREFIX_PATH ${QT_PATH} ${WIN_OPENGL_LIB_PATH})
