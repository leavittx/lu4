/*!
 *\file LU4ListViewWidget.h
 *\brief This file contains LU4ListViewWidget class declaration
 */

#ifndef LU4_LIST_VIEW_WIDGET_H
  #define LU4_LIST_VIEW_WIDGET_H

  #include <QListWidget>

  #include "LU4.h"

  /*!
   *\class LU4ListViewWidget
   *\brief List view widget
   */
  class LU4ListViewWidget : public QListWidget
  {
    Q_OBJECT
  public:
    /*!
     *\brief Constructor
     *\param widget Parent widget
     */
    explicit LU4ListViewWidget(QWidget* parent = nullptr);

    /*!
     *\brief Destructor
     */
    virtual ~LU4ListViewWidget() OVERRIDE;

  public slots:
    void onSwipeCard(const QString& id);

  private slots:
    void onCurrentItemChanged(QListWidgetItem* current, QListWidgetItem* privious);

  private:
    void addItem(const QString& name, bool isNew = true);
  };

#endif // LU4_LIST_VIEW_WIDGET_H