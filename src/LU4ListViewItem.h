/*!
 *\file LU4ListViewWidget.h
 *\brief This file contains LU4ListViewItem class declaration
 */

#ifndef LU4_LIST_VIEW_ITEM_H
  #define LU4_LIST_VIEW_ITEM_H

  #include <QListWidgetItem>

  #include "LU4.h"

  class LU4ListViewItemWidget;

  /*!
   *\class LU4ListViewItem
   *\brief Widget for list view item
   */
  class LU4ListViewItem : public QObject,
                          public QListWidgetItem
  {
    Q_OBJECT
  public:
    /*!
     *\brief Constructor
     */
    LU4ListViewItem(const QString& name, bool isNew, QListWidget* parent);
    /*!
     *\brief Destructor
     */
    virtual ~LU4ListViewItem();

    void setNew(bool isNew = true);
    bool getNew() const;

  private slots:
    void onCopy();

  private:
    void updateColor();

  private:
    //! Item widget
    LU4ListViewItemWidget*  mWidget;
    //! Name
    QString                 mName;
    //! Item is new flag
    bool                    mIsNew;
  };

#endif // LU4_LIST_VIEW_ITEM_H