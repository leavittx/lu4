#include <QMainWindow>
#include <QtGui>
#include <qapplication.h>
#include <QSystemTrayIcon>
#include <QMessageBox>
#include <QSystemTrayIcon>

#include "MainWindow.h"

#include <stdio.h>
#include <tchar.h>
#include <string>

#ifdef UNICODE
#define tstring wstring
#else
#define tstring string
#endif

#include "ZGuard.h"
#include "ZPort.h"

LPCTSTR PortTypeStrs[] = {
  TEXT("Unknown"),
  TEXT("COM"),
  TEXT("FT"),
  TEXT("IP")
};

INT g_nPortCount;

BOOL CALLBACK ZNotifyCB(UINT nMsg, LPARAM lMsgParam, PVOID pParam)
{
  switch (nMsg)
  {
  case ZP_N_INSERT:
    {
      PZP_PORT_INFO pInf1 = PZP_PORT_INFO(lMsgParam);

      _tprintf(TEXT("Converter insert: %s %s (%s); %s\n"), 
        PortTypeStrs[pInf1->nType],
        pInf1->szName,
        pInf1->szFriendly,
        pInf1->fBusy ? TEXT("busy") : TEXT(""));
    }
    break;
  case ZP_N_REMOVE:
    {
      PZP_PORT_INFO pInf1 = PZP_PORT_INFO(lMsgParam);

      _tprintf(TEXT("Converter remove: %s %s (%s)\n"), 
        PortTypeStrs[pInf1->nType],
        pInf1->szName,
        pInf1->szFriendly);
    }
    break;
  case ZP_N_STATE_CHANGED:
    {
      PZP_N_CHANGE_STATE pInf1 = PZP_N_CHANGE_STATE(lMsgParam);
      std::tstring s;
      if (pInf1->nChangeMask & 1)
        s = _T("busy, ");
      if (pInf1->nChangeMask & 2)
        s.append(_T("friendly, "));
      if (!s.empty())
        s.erase(s.length() - 2, 2);
      _tprintf(TEXT("State changed (%s): %s %s (%s); %s\n"), 
        s.c_str(),
        PortTypeStrs[pInf1->rInfo.nType],
        pInf1->rInfo.szName,
        pInf1->rInfo.szFriendly,
        pInf1->rInfo.fBusy ? TEXT("busy") : TEXT(""));
    }
    break;
  }
  return TRUE;
}


BOOL CALLBACK SerialPortsEnum(PZP_PORT_INFO pInfo, PVOID pParam)
{
  _ASSERT(pInfo != NULL);
  g_nPortCount++;
  _tprintf(TEXT("%d. %s %s (%s); %s\n"), 
    g_nPortCount, 
    PortTypeStrs[pInfo->nType],
    pInfo->szName,
    pInfo->szFriendly,
    pInfo->fBusy ? TEXT("busy") : TEXT(""));
  return TRUE;
}

int enumSerialPorts()
{
  DWORD nVersion = ZG_GetVersion();
  DWORD nVerMajor, nVerMinor, nVerBuild;

  nVerMajor = nVersion & 0xff;
  nVerMinor = (nVersion >> 8) & 0xff;
  nVerBuild = (nVersion >> 16) & 0xff;
  _tprintf(TEXT("SDK Guard v%d.%d.%d\n"), nVerMajor, nVerMinor, nVerBuild);
  if ((nVerMajor != ZG_SDK_VER_MAJOR) || (nVerMinor != ZG_SDK_VER_MINOR))
  {
    _tprintf(TEXT("Wrond version SDK Guard.\n"));
    getchar();
    return 0;
  }

  ZG_STATUS nRet;
  nRet = ZG_Initialize(ZP_IF_NO_MSG_LOOP);
  if (nRet < 0)
  {
    _tprintf(TEXT("ZG_Initialize fail: %d\n"), nRet);
    getchar();
    return 0;
  }
  __try
  {
    _tprintf(TEXT("Enum serial ports...\n"));
    g_nPortCount = 0;
    nRet = ZG_EnumSerialPorts(SerialPortsEnum, NULL);
    if (nRet < 0)
    {
      _tprintf(TEXT("ZG_EnumSerialPorts fail: %d\n"), nRet);
      getchar();
      return 0;
    }
    _tprintf(TEXT("--------------\n"));
    if (g_nPortCount > 0)
      _tprintf(TEXT("Found %d ports.\n"), g_nPortCount);
    else
      _tprintf(TEXT("Ports not found.\n"));

    HANDLE hNotify;
    _ZP_NOTIFY_SETTINGS rNS = {0};
    rNS.nNMask = ZP_NF_EXIST | ZP_NF_BUSY;
    rNS.pfnCallback = ZNotifyCB;
    nRet = ZG_FindNotification(&hNotify, &rNS, TRUE, FALSE);
    if (nRet < 0)
    {
      _tprintf(TEXT("ZG_FindNotification fail: %d\n"), nRet);
      getchar();
      return 0;
    }
    _tprintf(TEXT("Wait events...\n"));
    getchar();
    ZG_CloseNotification(hNotify);
  }
  __finally
  {
    ZG_Finalyze();
  }

  return 0;
}

int main(int argc, char *argv[])
{
  //Q_INIT_RESOURCE(systray);

  QApplication app(argc, argv);

  //enumSerialPorts();

  //if (!QSystemTrayIcon::isSystemTrayAvailable()) {
  //  QMessageBox::critical(0, QObject::tr("Systray"),
  //    QObject::tr("I couldn't detect any system tray "
  //    "on this system."));
  //  return 1;
  //}
  //QApplication::setQuitOnLastWindowClosed(false);

  MainWindow mainWindow;
  mainWindow.initialize(argc, argv);
  mainWindow.show();

  return app.exec();
}