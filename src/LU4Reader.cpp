/*!
 *\file LU4Reader.cpp
 *\brief This file contains LU4Reader class declaration
 */

#include "LU4Reader.h"

#include <cstdio>

LPCTSTR CtrTypeStrs[] = {
  TEXT(""),
  TEXT("Gate 2000"),
  TEXT("Matrix II Net"),
  TEXT("Z5R Net"),
  TEXT("Z5R Net 8000"),
  TEXT("Guard Net"),
  TEXT("Z9")
};

LPCTSTR KeyModeStrs[] = {
  TEXT("Touch Memory"),
  TEXT("Proximity")
};

LPCTSTR EvTypeStrs[] = {
  TEXT(""),
  TEXT("BUT_OPEN"),			// ������� ������� �������
  TEXT("KEY_NOT_FOUND"),		// ���� �� ������ � ����� ������
  TEXT("KEY_OPEN"),			// ���� ������, ����� �������
  TEXT("KEY_ACCESS"),			// ���� ������, ������ �� ��������
  TEXT("REMOTE_OPEN"),		// ������� ���������� �� ����
  TEXT("KEY_DOOR_BLOCK"),		// ���� ������, ����� �������������
  TEXT("BUT_DOOR_BLOCK"),		// ������� ������� ��������������� ����� �������
  TEXT("NO_OPEN"),			// ����� ��������
  TEXT("NO_CLOSE"),			// ����� ��������� �������� (timeout)
  TEXT("PASSAGE"),			// ������ ���������
  TEXT("SENSOR1"),			// �������� ������ 1
  TEXT("SENSOR2"),			// �������� ������ 2
  TEXT("REBOOT"),				// ������������ �����������
  TEXT("BUT_BLOCK"),			// ������������� ������ ����������
  TEXT("DBL_PASSAGE"),		// ������� �������� �������
  TEXT("OPEN"),				// ����� ������� ������
  TEXT("CLOSE"),				// ����� �������
  TEXT("POWEROFF"),			// ������� �������
  TEXT("ELECTRO_ON"),			// ��������� ��������������
  TEXT("ELECTRO_ON"),			// ��������� ��������������
  TEXT("ELECTRO_OFF"),		// ���������� ��������������
  TEXT("LOCK_CONNECT"),		// ��������� ����� (�������)
  TEXT("LOCK_DISCONNECT"),	// ���������� ����� (�������)
  TEXT("FIRE_STATE"),			// ��������� ��������� ������
  TEXT("SECUR_STATE"),		// ��������� ��������� ������
  TEXT("UNKNOWN_NUMBER"),		// ����������� ����
  TEXT("GATEWAY_PASS"),		// �������� ���� � ����
  TEXT("GATEWAY_BLOCK"),		// ������������ ���� � ���� (�����)
  TEXT("GATEWAY_ALLOWED"),	// �������� ���� � ����
  TEXT("ANTIPASSBACK")		// ������������ ������ (�����������)
};

LPCTSTR DirectStrs[] = {
  TEXT(""),
  TEXT("IN"),   // ����
  TEXT("OUT")   // �����
};

LPCTSTR EcSubEvStrs[] = {
  TEXT(""),
  TEXT("CARD_DELAY"),			// ��������� �������� ����� � ������ ������� (��� �����) �������� ��������
  TEXT("RESERVED1"),			// (���������������)
  TEXT("ON_NET"),				// �������� �������� �� ����
  TEXT("OFF_NET"),			// ��������� �������� �� ����
  TEXT("ON_SCHED"),			// �������� �� ��������� ����
  TEXT("OFF_SHED"),			// ��������� �� ��������� ����
  TEXT("CARD"),				// ��������� �������� ����� � ������������ ����������
  TEXT("RESERVED2"),			// (���������������)
  TEXT("OFF_TIMEOUT"),		// ��������� ����� ��������� ��������
  TEXT("OFF_EXIT")			// ��������� �� ������������ ������� ������
};

LPCTSTR FireSubEvStrs[] = {
  TEXT(""),
  TEXT("OFF_NET"),			// ��������� �� ����
  TEXT("ON_NET"),				// �������� �� ����
  TEXT("OFF_INPUT_F"),		// ��������� �� ����� FIRE
  TEXT("ON_INPUT_F"),			// �������� �� ����� FIRE
  TEXT("OFF_TEMP"),			// ��������� �� ������� �����������
  TEXT("ON_TEMP")				// �������� �� ������� �����������
};

LPCTSTR SecurSubEvStrs[] = {
  TEXT(""),
  TEXT("OFF_NET"),			// ��������� �� ����
  TEXT("ON_NET"),				// �������� �� ����
  TEXT("OFF_INPUT_A"),		// ��������� �� ����� ALARM
  TEXT("ON_INPUT_A"),			// �������� �� ����� ALARM
  TEXT("OFF_TAMPERE"),		// ��������� �� �������
  TEXT("ON_TAMPERE"),			// �������� �� �������
  TEXT("OFF_DOOR"),			// ��������� �� ������� �����
  TEXT("ON_DOOR")				// �������� �� ������� �����
};

//throw std::runtime_error(msg);
#define CHECK_SUCCESS(expr, msg) \
  if (expr != ZG_SUCCESS) { \
    fprintf(stderr, "%s failed\n", msg); \
    return false; \
  }

LU4ZGuardReader::LU4ZGuardReader(ZP_PORT_TYPE portType, LPCWSTR portName, int controlAddress)
  : mPortType(portType), mPortName(portName), mControlAddress(controlAddress)
{

}

LU4ZGuardReader::~LU4ZGuardReader()
{

}


tstring LU4ZGuardReader::ZKeyNumToStr(const Z_KEYNUM& rNum)
{
  tstring s;

  if (mProximity)
  {
    int n;
    s.resize(128);
    n = _stprintf_s(&s[0], s.size(), TEXT("%d,%d"), rNum[3], *(PWORD)&rNum[1]);
    s.resize(n);
  }
  else
  {
    INT i, j;
    s.resize(rNum[0] * 2 + 1);
    j = 0;
    for (i = rNum[0]; i > 0; i--) {
      _stprintf_s(&s[j], s.size() - j, TEXT("%.2X"), rNum[i]);
      j += 2;
    }
    s.resize(rNum[0] * 2);
  }
  return s;
}

void LU4ZGuardReader::ShowEvents(int nStart, int nCount)
{
  _ZG_CTR_EVENT aEvents[6];
  PZG_CTR_EVENT pEv;
  int i, j, nIdx, nCnt;
  ZG_STATUS status;

  i = 0;
  while (i < nCount)
  {
    nIdx = (nStart + i) % mControlMaxEvents; 
    nCnt = (nCount - i);
    if (nCnt > _countof(aEvents))
      nCnt = _countof(aEvents);

    if ((nIdx + nCnt) > mControlMaxEvents)
      nCnt = (mControlMaxEvents - nIdx);

    status = ZG_Ctr_ReadEvents(mControlHandle, nIdx, aEvents, nCnt, NULL, NULL);
    if (status < 0)
    {
      _tprintf(TEXT("ZG_Ctr_Open fail: %d\n"), status);
      getchar();
      return;
    }
    for (j = 0; j < nCnt; j++)
    {
      pEv = &aEvents[j];
      switch (pEv->nType)
      {
      case ZG_EV_ELECTRO_ON:
      case ZG_EV_ELECTRO_OFF:
        {
          _ZG_EV_TIME rTime;
          ZG_EC_SUB_EV nSubEvent;
          DWORD nPowerFlags;
          ZG_Ctr_DecodeEcEvent(mControlHandle, pEv->aData, &rTime, &nSubEvent, &nPowerFlags);
          _tprintf(TEXT("%.4d. %.2d.%.2d %.2d:%.2d:%.2d %s Sub_event: %s, Power flags: %d\n"),
            nIdx + j,
            rTime.nDay, rTime.nMonth,
            rTime.nHour, rTime.nMinute, rTime.nSecond,
            EvTypeStrs[pEv->nType],
            EcSubEvStrs[nSubEvent], nPowerFlags);
        }
        break;
      case ZG_EV_FIRE_STATE:
        {
          _ZG_EV_TIME rTime;
          ZG_FIRE_SUB_EV nSubEvent;
          DWORD nFireFlags;
          ZG_Ctr_DecodeFireEvent(mControlHandle, pEv->aData, &rTime, &nSubEvent, &nFireFlags);
          _tprintf(TEXT("%.4d. %.2d.%.2d %.2d:%.2d:%.2d %s Sub_event: %s, Fire flags: %d\n"),
            nIdx + j,
            rTime.nDay, rTime.nMonth,
            rTime.nHour, rTime.nMinute, rTime.nSecond,
            EvTypeStrs[pEv->nType],
            FireSubEvStrs[nSubEvent], nFireFlags);
        }
        break;
      case ZG_EV_SECUR_STATE:
        {
          _ZG_EV_TIME rTime;
          ZG_SECUR_SUB_EV nSubEvent;
          DWORD nSecurFlags;
          ZG_Ctr_DecodeSecurEvent(mControlHandle, pEv->aData, &rTime, &nSubEvent, &nSecurFlags);
          _tprintf(TEXT("%.4d. %.2d.%.2d %.2d:%.2d:%.2d %s Sub_event: %s, Security flags: %d\n"),
            nIdx + j,
            rTime.nDay, rTime.nMonth,
            rTime.nHour, rTime.nMinute, rTime.nSecond,
            EvTypeStrs[pEv->nType],
            SecurSubEvStrs[nSubEvent], nSecurFlags);
        }
        break;
      case ZG_EV_UNKNOWN_KEY:
        {
          Z_KEYNUM rNum;
          ZG_Ctr_DecodeUnkKeyEvent(mControlHandle, pEv->aData, &rNum);
          _tprintf(TEXT("%.4d. Key \"%s\"\n"),
            nIdx + j,
            ZKeyNumToStr(rNum).c_str());
        }
        break;
      default:
        {
          _ZG_EV_TIME rTime;
          ZG_CTR_DIRECT nDirect;
          INT nKeyIdx, nKeyBank;
          ZG_Ctr_DecodePassEvent(mControlHandle, pEv->aData, &rTime, &nDirect, &nKeyIdx, &nKeyBank);
          _tprintf(TEXT("%.4d. %.2d.%.2d %.2d:%.2d:%.2d %s %s (key_idx: %d, bank#: %d)\n"),
            nIdx + j,
            rTime.nDay, rTime.nMonth,
            rTime.nHour, rTime.nMinute, rTime.nSecond,
            DirectStrs[nDirect],
            EvTypeStrs[pEv->nType],
            nKeyIdx, nKeyBank);

          if (pEv->nType == ZG_EV_KEY_OPEN)
          {
            emit swipeCard(QString::number(nKeyIdx));
          }
        }
        break;
      }
    }
    i += nCnt;
  }
}


namespace
{
  BOOL CALLBACK ZNotifyCB(UINT nMsg, LPARAM lMsgParam, PVOID pUserData)
  {                        
    LU4ZGuardReader* obj = reinterpret_cast<LU4ZGuardReader*>(pUserData);
  
    obj->eventCallback(nMsg, lMsgParam);

    return TRUE;
  }
} // namespace 'anonymous'


void LU4ZGuardReader::eventCallback(UINT nMsg, LPARAM lMsgParam)
{
  switch (nMsg)
  {
  case ZG_N_CTR_NEW_EVENT:
    {
      PZG_N_NEW_EVENT_INFO pInfo = PZG_N_NEW_EVENT_INFO(lMsgParam);

      _tprintf(TEXT("==> New Events: %d\n"), pInfo->nNewCount);
      ShowEvents(pInfo->nReadIdx, pInfo->nNewCount);
      mReadEventIndex = pInfo->nWriteIdx;
    }
    break;
  }
}


void LU4ZGuardReader::enableEventNotifyer()
{
  ZG_STATUS status;
  _ZG_CTR_NOTIFY_SETTINGS rNS = {0};
  rNS.nNMask = ZG_NF_CTR_NEW_EVENT;
  rNS.pfnCallback = ZNotifyCB;
  rNS.pUserData = reinterpret_cast<void*>(this);
  rNS.nReadEvIdx = mReadEventIndex;
  rNS.nCheckStatePeriod = 300;
  status = ZG_Ctr_FindNotification(mControlHandle, &rNS);
  if (status < 0)
  {
    _tprintf(TEXT("ZG_Ctr_FindNotification fail: %d\n"), status);
    getchar();
    return;
  }
}


bool LU4ZGuardReader::connect()
{
  ZG_STATUS status;
  HANDLE handle = 0;

  status = ZG_Initialize(ZP_IF_NO_MSG_LOOP);
  CHECK_SUCCESS(status, "ZG_Initialize()");

  _ZG_CVT_OPEN_PARAMS rOp;
  ZeroMemory(&rOp, sizeof(rOp));
  rOp.nType = mPortType;
  rOp.pszName = mPortName;
  status = ZG_Cvt_Open(&handle, &rOp);
  CHECK_SUCCESS(status, "ZG_Cvt_Open()");

  _ZG_CTR_INFO rCtrInfo;
  ZeroMemory(&rCtrInfo, sizeof(rCtrInfo));
  status = ZG_Ctr_Open(&mControlHandle, handle, mControlAddress, 0, &rCtrInfo);
  CHECK_SUCCESS(status, "ZG_Ctr_Open()");

  mControlMaxEvents = rCtrInfo.nMaxEvents;
  mProximity = (rCtrInfo.nFlags & ZG_CTR_F_PROXIMITY) != 0;
  mControlFlags = rCtrInfo.nFlags;
  _tprintf(TEXT("%s addr: %d, s/n: %d, v%d.%d, Max_Events: %d, Key_Type: %s\n"),
    CtrTypeStrs[rCtrInfo.nType],
    rCtrInfo.nAddr,
    rCtrInfo.nSn,
    LOBYTE(rCtrInfo.nVersion), HIBYTE(rCtrInfo.nVersion),
    rCtrInfo.nMaxEvents,
    KeyModeStrs[mProximity ? 1 : 0]);

  INT nWrIdx, nRdIdx;
  status = ZG_Ctr_ReadEventIdxs(mControlHandle, &nWrIdx, &nRdIdx);
  CHECK_SUCCESS(status, "ZG_Ctr_ReadEventIdxs()");

  mReadEventIndex = nWrIdx;


  enableEventNotifyer();

  return true;
}

