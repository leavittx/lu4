/*!
 *\file LU4.h
 *\brief This is a global header file, that includes configuration header and contains most common macro defines and params
 */

#ifndef LU4_H
  #define LU4_H

  //! It's necessary to use override and sealed operator, when overriding virtual method
  //! We use safe macro definition, in case someone will ever build this project under older versions of MSVC
  #if _MSC_VER >= 1400
  #define OVERRIDE override
  #define SEALED   sealed
  #else
  #define OVERRIDE
  #define SEALED
  #endif // _MSC_VER >= 1400

#endif // LU4_H