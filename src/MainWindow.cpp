/*!
 *\file MainWindow.cpp
 *\brief This file contains application main window implementation
 */

#include "LU4MainWidget.h"
#include "MainWindow.h"

MainWindow::MainWindow()
  : QMainWindow(nullptr),
    mMainWidget(nullptr)
{
	
}

MainWindow::~MainWindow()
{
  
}

void MainWindow::initialize(int argc, char *argv[])
{
  // Delete window, when it's going to be closed
  setAttribute(Qt::WA_DeleteOnClose, true);
  // Init GUI
  mMainWidget = new LU4MainWidget(this);
  setCentralWidget(mMainWidget);
}
                                
void MainWindow::closeEvent( QCloseEvent* event )
{
  
}

void MainWindow::mousePressEvent( QMouseEvent* event )
{
  
}

void MainWindow::mouseReleaseEvent( QMouseEvent* event )
{
  
}

void MainWindow::mouseDoubleClickEvent( QMouseEvent* event )
{
  
}

void MainWindow::mouseMoveEvent( QMouseEvent* event )
{
  
}

void MainWindow::wheelEvent( QWheelEvent* event )
{
  
}

void MainWindow::keyPressEvent( QKeyEvent* event )
{
  
}

void MainWindow::keyReleaseEvent( QKeyEvent* event )
{
  
}