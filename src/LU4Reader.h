/*!
 *\file LU4AbstractReader.h
 *\brief This file contains LU4AbstractReader class declaration
 */

#ifndef LU4_READER_H
  #define LU4_READER_H

  #include <QObject>

  #include "LU4.h"

  //! Z-XXX stuff
  #include <stdio.h>
  #include <tchar.h>
  #include <string>
  #include "ZGuard.h"
  #include "ZPort.h"

  #ifdef UNICODE
  #define tstring std::wstring
  #else
  #define tstring std::string
  #endif

  /*!
   *\class LU4AbstractReader
   */
  class LU4AbstractReader : public QObject
  {
    Q_OBJECT
  public:
    /*!
     *\brief Constructor
     */
    LU4AbstractReader() { }

    /*!
     *\brief Destructor
     */
    virtual ~LU4AbstractReader() = 0 { }

  public:
    /*!
     *\brief Connect to reader
     */
    virtual bool connect() = 0;

  signals:
    void swipeCard(const QString& id);
  };

  /*!
   *\class LU4ZGuardReader
   */
  class LU4ZGuardReader : public LU4AbstractReader
  {
    Q_OBJECT
  public:
    /*!
     *\brief Constructor
     */
    explicit LU4ZGuardReader(ZP_PORT_TYPE portType, LPCWSTR portName, int controlAddress);

    /*!
     *\brief Destructor
     */
    virtual ~LU4ZGuardReader() OVERRIDE;

  public:
    /*!
     *\brief Connect to reader
     */
    virtual bool connect() OVERRIDE;

    void eventCallback(UINT nMsg, LPARAM lMsgParam);

  private:
    tstring ZKeyNumToStr(const Z_KEYNUM& rNum);
    void ShowEvents(int nStart, int nCount);
    void enableEventNotifyer();

  private:
    //! Port type
    ZP_PORT_TYPE    mPortType;
    //! Port name
    LPCWSTR         mPortName;
    //! Control address
    int             mControlAddress;

    HANDLE          mControlHandle;
    int             mReadEventIndex;
    int             mControlMaxEvents;
    bool            mProximity;
    uint            mControlFlags;
  };

#endif // LU4_READER_H