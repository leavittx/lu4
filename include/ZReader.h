#pragma once
// ����������� ���� ���� ifdef - ��� ����������� ����� �������� ��������, ���������� ��������� 
// �������� �� ��������� DLL. ��� ����� ������ DLL �������������� � �������������� ������� ZREADER_EXPORTS,
// � ��������� ������. ���� ������ �� ������ ���� ��������� � �����-���� �������
// ������������ ������ DLL. ��������� ����� ����� ������ ������, ��� �������� ����� �������� ������ ����, ����� 
// ������� ZREADER_API ��� ��������������� �� DLL, ����� ��� ������ DLL ����� �������,
// ������������ ������ ��������, ��� ����������������.

#ifndef ZREADER_LINKONREQUEST

#ifdef ZREADER_EXPORTS
#define ZREADER_API(type) extern "C"  __declspec(dllexport) type __stdcall 
#else
#ifdef ZREADER_STATIC
#define ZREADER_API(type) extern "C" type __stdcall 
#else
#define ZREADER_API(type) extern "C" __declspec(dllimport) type __stdcall
#endif
#endif
#endif

#include "ZPort.h"
#include "ZBase.h"

#define ZR_SDK_VER_MAJOR	3
#define ZR_SDK_VER_MINOR	18

#pragma pack(1)   // turn byte alignment on

typedef INT ZR_STATUS;	// ��������� ���������� ������� DLL

#define ZR_SUCCESS						0	// �������� ��������� �������
#define ZR_E_CANCELLED					1	// �������� �������������
#define ZR_E_NOT_FOUND					2	// �� ������

#define ZR_E_INVALID_PARAM				-1	// ������������ ���������
#define ZR_E_OPEN_NOT_EXIST				-2	// ���� �� ����������
#define ZR_E_OPEN_ACCESS				-3	// ���� ����� ������ ����������
#define ZR_E_OPEN_PORT					-4	// ������ ������ �������� �����
#define ZR_E_PORT_IO_ERROR				-5	// ������ ����� (��������� �������� �� USB?)
#define ZR_E_PORT_SETUP					-6	// ������ ��������� �����
#define ZR_E_LOAD_FTD2XX				-7	// ��������� ��������� FTD2XX.DLL
#define ZR_E_INIT_SOCKET				-8	// �� ������� ���������������� ������
#define ZR_E_SERVERCLOSE				-9	// ���������� ������ �� ������� �������
#define ZR_E_NOT_ENOUGH_MEMORY			-10	// ������������ ������ ��� ��������� �������
#define ZR_E_UNSUPPORT					-11	// ������� �� ��������������
#define ZR_E_NOT_INITALIZED				-12	// �� ������������������� � ������� ZR_Initialize
#define ZR_E_CREATE_EVENT				-13	// ������ ������� CreateEvent

#define ZR_E_INSUFFICIENT_BUFFER		-101	// ������ ������ ������� ���
#define ZR_E_NO_ANSWER					-102	// ��� ������
#define ZR_E_BAD_ANSWER					-103	// �������������� �����
#define ZR_E_CARD_NOT_SELECT			-104	// ����� �� ������� � ������� ������� ZR_Rd_SelectCard
#define ZR_E_NO_CARD					-105	// ����� �� ����������
#define ZR_E_WRONG_ZPORT_VERSION		-107	// �� ���������� ������ ZPort.dll

#define ZR_E_RD_OTHER					-200	// ������ ������ (����� �����������)
#define ZR_E_CARD_ACCESS				-201	// ��� ������� � �����
#define ZR_E_PAGE_LOCK					-202	// �������� �������������

#define ZR_E_NO_T57						-203	// ����� �� ���� T5557
#define ZR_E_NO_WRITE_T57				-204	// �� ������� �������� �� T5557 (���� ������������ ��� ������ ��� ������ ������������ ������)

#define ZR_E_INV_MODEL					-205	// �������������� ������ (��� ��������)
#define ZR_E_INV_BOOTFILE				-206	// ������������ ������ ��������
#define ZR_E_BUFFER_OVERFLOWN			-207	// ����� ����������
#define ZR_E_KEY_NOT_FOUND				-208	// ���������� ���� �� ������

#define ZR_E_MIF_FCS					-301	// ������ FCS
#define ZR_E_MIF_INV_CMD				-302	// �������� �������
#define ZR_E_MIF_INV_PAR				-303	// �������� ������
#define ZR_E_MIF_RES					-304	// ������� ���������� ���������
#define ZR_E_MIF_RD_DOWN				-305	// ���������� ������ ��� ���������
#define ZR_E_MIF_NO_CARD				-306	// �������� �������� �� ����������
#define ZR_E_MIF_CD_ANSWER				-307	// �������� ���� �������������� �����
#define ZR_E_MIF_AUTH					-308	// ������ ����������� �����
#define ZR_E_MIF_CD_NACK				-309	// ����� �������� � ���������� �������
#define ZR_E_MIF_PREV_CMD_NC			-310	// ���������� ������� �� ���������

#define ZR_E_OTHER						-1000	// ������ ������

#define ZR_DEVTYPE_CVT					0
#define ZR_DEVTYPE_Z2U					3
#define ZR_DEVTYPE_Z2M					4

#ifndef ZG_DEVTYPE_GUARD
#define ZG_DEVTYPE_GUARD				1
#endif

// ����� ��� _ZR_RD_NOTIFY_SETTINGS.nNMask
#define ZR_RNF_PLACE_CARD				1		// ZR_RN_CARD_INSERT / ZR_RN_CARD_REMOVE
#define ZR_RNF_INPUT_CHANGE				2		// ZR_RN_INPUT_CHANGE
#define ZR_RNF_IND_FLASH_END			4		// ZR_RN_IND_FLASH_END
#define ZR_RNF_WND_SYNC					0x4000	// ���������������� � �������� ��������� Windows
// (�� �������� ���� ��� ������������� ���������� ���� ZP_IF_NO_MSG_LOOP)
#define ZR_RNF_ONLY_NOTIFY				0x8000	// ������ ���������� � ���������� ����� ��������� � �������

// ����������� ������� ZR_FindNotification
#define ZR_RN_CARD_INSERT				100	// ����� ��������� ((PZR_CARD_INFO)nMsgParam)
#define ZR_RN_CARD_REMOVE				101 // ����� ������� ((PZR_CARD_INFO)nMsgParam, ����� ���� = NULL)
#define ZR_RN_CARD_UNKNOWN				102	// ����������� ��������� �� �����������, (LPWCSTR)nMsgParam - ����� ���������
#define ZR_RN_INPUT_CHANGE				103 // ���������� ��������� ������ Matrix III Net (nMsgParam - ����� ��������� ������)
#define ZR_RN_IND_FLASH_END				104	// ������� ��������� ��������� (Z-2 USB MF, CPZ-2-MF, Matrix III Net), ��� ���������

// ��� �����������
enum ZR_RD_TYPE: INT
{
	ZR_RD_UNDEF = 0,
	ZR_RD_Z2U,				// Z-2 USB
	ZR_RD_M3A,				// Matrix III Rd-All
	ZR_RD_Z2M,				// Z-2 USB MF
	ZR_RD_M3N,				// Matrix III Net
	ZR_RD_CPZ2MF,			// CP-Z-2MF
	ZR_RD_Z2EHR,			// Z-2 EHR
	ZR_RD_Z2BASE,			// Z-2 Base
	ZR_RD_M5				// Matrix V
};

// ��������� ����������� �����������
typedef struct _ZR_RD_NOTIFY_SETTINGS
{
	UINT nNMask;				// ����� ����� ����������� (ZR_RNF_..)

	ZP_NOTIFYPROC pfnCallback;	// Callback-�������
	PVOID pUserData;			// �������� ��� Callback-�������

	UINT nCheckCardPeriod;		// ������ ������������ ���� � ���� ����������� Z2USB MF � ����������� Matrix III Net � �� (���� =0, ������������ �������� ��-���������, 300)
	UINT nCheckInputPeriod;		// ������ �������� ��������� ������ ��� Matrix III Net (���� =0, ������������ �������� ��-���������, �������)
} *PZR_RD_NOTIFY_SETTINGS;

// ���������� � �����������
typedef struct _ZR_RD_INFO : _ZP_DEVICE_INFO
{
	ZR_RD_TYPE nType;		// ��� �����������

	LPWSTR pszLinesBuf;		// ����� ��� �������������� �����
	INT nLinesBufMax;		// ������ ������ � ��������, ������� ����������� '\0'
} *PZR_RD_INFO;

// ��������� �������� ����������� (��� ������� ZR_Rd_Open)
typedef struct _ZR_RD_OPEN_PARAMS
{
	ZP_PORT_TYPE nType;			// ��� �����
	LPCWSTR pszName;			// ��� �����. ���� =NULL, �� ������������ hPort
	HANDLE hPort;				// ���������� �����, ���������� �������� ZP_Open
	ZR_RD_TYPE nRdType;			// ��� �����������. ���� =ZR_RD_UNDEF, �� ���������������
	PZP_WAIT_SETTINGS pWait;	// ��������� ��������. ����� ���� =NULL.
	BYTE nStopBits;
	INT nMode;
} *PZR_RD_OPEN_PARAMS;

// ��� �����
enum ZR_CARD_TYPE: INT
{
	ZR_CD_UNDEF = 0,
	ZR_CD_EM,				// Em-Marine
	ZR_CD_HID,				// Hid
	ZR_CD_IC,				// iCode
	ZR_CD_UL,				// Mifare UltraLight
	ZR_CD_1K,				// Mifare Classic 1K
	ZR_CD_4K,				// Mifare Classic 4K
	ZR_CD_DF,				// Mifare DESFire
	ZR_CD_PX,				// Mifare ProX
	ZR_CD_COD433F,			// Cod433 Fix
	ZR_CD_COD433,			// Cod433
	ZR_CD_DALLAS,			// Dallas
	ZR_CD_CAME433,			// ����������� CAME
	ZR_CD_PLUS,				// Mifare Plus
	ZR_CD_PLUS1K,			// Mifare Plus 1K
	ZR_CD_PLUS2K,			// Mifare Plus 2K
	ZR_CD_PLUS4K,			// Mifare Plus 4K
	ZR_CD_MINI				// Mifare Mini
};

typedef struct _ZR_CARD_INFO
{
	ZR_CARD_TYPE nType;		// ��� �����
	Z_KEYNUM nNum;			// ����� �����
} *PZR_CARD_INFO;

// ����� ��� ������� ZR_Rd_FindT57 \ ZR_Rd_ReadT57Block \ ZR_Rd_WriteT57Block
#define ZR_T57F_INIT		1	// ��������� ����������������, ���� �� ������� ��������� ��������� (������ � ZR_Rd_FindT57)
#define ZR_T57F_PSW			2	// ������������ ������
#define ZR_T57F_BLOCK		4	// ����������� ���������� ���������� ����� (������ � ZR_Rd_WriteT57Block)

// Modulation
#define T57_MOD_DIRECT			0    // 0 0 0 0 0
#define T57_MOD_PSK1			2    // 0 0 0 1 0
#define T57_MOD_PSK2			4    // 0 0 1 0 0
#define T57_MOD_PSK3			6    // 0 0 1 1 0
#define T57_MOD_FSK1			8    // 0 1 0 0 0
#define T57_MOD_FSK2			0xA  // 0 1 0 1 0
#define T57_MOD_FSK1A			0xC  // 0 1 1 0 0
#define T57_MOD_FSK2A			0xE  // 0 1 1 1 0
#define T57_MOD_MANCHESTER		0x10 // 1 0 0 0 0
#define T57_MOD_BIPHASE50		0x1  // 0 0 0 0 1
#define T57_MOD_BIPHASE57		0x11 // 1 0 0 0 1
// PSK CF
enum ZR_T57_PSK: INT
{
	T57_PSK_UNDEF = -1,
	T57_PSK_RF2,	// 0 0
	T57_PSK_RF4,	// 0 1
	T57_PSK_RF8,	// 1 0
	T57_PSK_RES		// 1 1
};

typedef struct _ZR_T57_CONFIG
{
	BOOL fXMode;			// True, ���� X-Mode, ����� - e5550 compatible
	UINT nMasterKey;
	UINT nDataRate;
	UINT nModulation;
	ZR_T57_PSK nPSK_CF;
	BOOL fAOR;
	BOOL fOTP;				// (������ � XMode)
	INT nMaxBlock;
	BOOL fPsw;				// True, ���� ������ ����������
	BOOL fST_Seq_Ter;		// (������ � e5550)
	BOOL fSST_Seq_StMrk;	// (������ � XMode)
	BOOL fFastWrite;
	BOOL fInverseData;		// (������ � XMode)
	BOOL fPOR_Delay;
} *PZR_T57_CONFIG;

typedef struct _ZR_M3N_CONFIG
{
	BYTE nWorkMode;
	BYTE nOutZumm;
	BYTE nOutTM;
	BYTE nOutExit;
	BYTE nOutLock;
	BYTE nOutDoor;
	BYTE nProt;
	BYTE nFlags;	// 0 Impulse, 1 No card, 2 card num
	BYTE nCardFormat;
	BYTE nSecurityMode;
	BYTE Reserved1[2];
} *PZR_M3N_CONFIG;

typedef UINT64 ZR_MF_AUTH_KEY;
typedef ZR_MF_AUTH_KEY near *PZR_MF_AUTH_KEY;

enum ZR_IND_STATE: INT
{
	ZR_IND_NO_CHANGE = 0,
	ZR_IND_ON,
	ZR_IND_OFF,
	ZR_IND_AUTO
};

// ����� ��������� ����������
#define ZR_ISF_ON		1
#define ZR_ISF_AUTO		2

typedef struct _ZR_IND_FLASH
{
	ZR_IND_STATE nRed;
	ZR_IND_STATE nGreen;
	ZR_IND_STATE nSound;
	DWORD nDuration; // ms
} *PZR_IND_FLASH;

#define ZR_MAX_IND_FLASH		15


typedef BOOL (CALLBACK* ZR_ENUMCARDSPROC) (PZR_CARD_INFO pInfo, PVOID pUserData);

typedef BOOL (CALLBACK* ZR_ENUMDRDPROC) (PZR_RD_INFO pInfo, PZP_PORT_INFO pPort, PVOID pUserData);
typedef BOOL (CALLBACK* ZR_PROCESSCALLBACK) (INT nPos, INT nMax, PVOID pUserData);

#pragma pack() // turn byte alignment off

#ifndef ZREADER_LINKONREQUEST

// ���������� ������ ���������� Z2Usb.dll
ZREADER_API(DWORD) ZR_GetVersion();

// �������������/����������� ����������
ZREADER_API(ZR_STATUS) ZR_Initialize(UINT nFlags);
ZREADER_API(ZR_STATUS) ZR_Finalyze();

// ����������� ��� ������������ �����������
#define ZR_EnumSerialPorts(pEnumProc, pUserData) (ZR_STATUS)ZP_EnumSerialPorts(0x19, pEnumProc, pUserData)
inline ZR_STATUS ZR_EnumReaders(PZP_PORT_ADDR pPorts, INT nPCount, 
		ZR_ENUMDRDPROC pEnumProc, PVOID pUserData, PZP_WAIT_SETTINGS pWait=NULL)
	{ return (ZR_STATUS)ZP_EnumSerialDevices(0x19, pPorts, nPCount, (ZP_ENUMDEVICEPROC)pEnumProc, pUserData, pWait); }
inline ZR_STATUS ZR_FindReader(PZP_PORT_ADDR pPorts, INT nPCount, 
		PZR_RD_INFO pInfo, PZP_PORT_INFO pPort, PZP_WAIT_SETTINGS pWait=NULL)
	{ return (ZR_STATUS)ZP_FindSerialDevice(0x19, pPorts, nPCount, pInfo, sizeof(_ZR_RD_INFO), pPort, pWait); }
// ����������� �����������
inline ZR_STATUS ZR_FindNotification(PHANDLE pHandle, PZP_NOTIFY_SETTINGS pSettings)
	{ pSettings->nSDevTypes |= 0x19; return (ZR_STATUS)ZP_FindNotification(pHandle, pSettings); }
#define ZR_CloseNotification(hHandle)	(ZR_STATUS)ZP_CloseNotification(hHandle)
#define ZR_ProcessMessages(hHandle, pEnumProc, pUserData)	(ZR_STATUS)ZP_ProcessMessages(hHandle, pEnumProc, pUserData)

// ��������� ����� �������� � �����������
ZREADER_API(ZR_STATUS) ZR_UpdateRdFirmware(PZR_RD_OPEN_PARAMS pParams,   
	LPCVOID pData, INT nCount, ZR_PROCESSCALLBACK pfnCallback, PVOID pUserData);

// ���������/��������� �����������
ZREADER_API(ZR_STATUS) ZR_Rd_Open(PHANDLE pHandle, PZR_RD_OPEN_PARAMS pParams, 
	PZR_RD_INFO pInfo=NULL);
ZREADER_API(ZR_STATUS) ZR_Rd_Close(HANDLE hHandle);
// ����������� �� �����������, �� �������� ����, ���������� ���������� �����, ���������� �������� ZP_Open
ZREADER_API(ZR_STATUS) ZR_Rd_DettachPort(HANDLE hHandle, PHANDLE pPortHandle);

// ����������/������������� ��������� �������� ���������� �������
ZREADER_API(ZR_STATUS) ZR_Rd_GetWaitSettings(HANDLE hHandle, PZP_WAIT_SETTINGS pSetting);
ZREADER_API(ZR_STATUS) ZR_Rd_SetWaitSettings(HANDLE hHandle, PZP_WAIT_SETTINGS pSetting);

// ��������� ���� (���������������� ������� �������� � ������)
ZREADER_API(ZR_STATUS) ZR_Rd_SetCapture(HANDLE hHandle);
// ��������� ���� (������������ ������� �������� � ������)
ZREADER_API(ZR_STATUS) ZR_Rd_ReleaseCapture(HANDLE hHandle);

// ���������� ���������� � �����������
ZREADER_API(ZR_STATUS) ZR_Rd_GetInformation(HANDLE hHandle, PZR_RD_INFO pInfo);

// ��������� � ����������� ����� ��������
ZREADER_API(ZR_STATUS) ZR_Rd_UpdateFirmware(HANDLE hHandle, LPCVOID pData, INT nCount, 
	ZR_PROCESSCALLBACK pfnCallback, PVOID pUserData);

// ����������� ����������� �����������
ZREADER_API(ZR_STATUS) ZR_Rd_SetNotification(HANDLE hHandle, PZR_RD_NOTIFY_SETTINGS pSettings);
ZREADER_API(ZR_STATUS) ZR_Rd_ProcessMessages(HANDLE hHandle, ZP_NOTIFYPROC pEnumProc, PVOID pUserData);

// ���� ����� � ���� �����������
ZREADER_API(ZR_STATUS) ZR_Rd_FindCard(HANDLE hHandle, ZR_CARD_TYPE* pType, Z_KEYNUM* pNum);
// ������/������ ����� Mifare UL
ZREADER_API(ZR_STATUS) ZR_Rd_ReadULCard4Page(HANDLE hHandle, INT nPageN, LPVOID pBuf16);
ZREADER_API(ZR_STATUS) ZR_Rd_WriteULCardPage(HANDLE hHandle, INT nPageN, LPCVOID pData4);


// Z2USB

ZREADER_API(ZR_STATUS) ZR_Rd_FindT57(HANDLE hHandle, Z_KEYNUM* pNum, PINT pPar, UINT nPsw, UINT nFlags);
ZREADER_API(ZR_STATUS) ZR_Rd_ReadT57Block(HANDLE hHandle, INT nBlockN, LPVOID pBuf4, INT nPar, UINT nPsw, UINT nFlags);
ZREADER_API(ZR_STATUS) ZR_Rd_WriteT57Block(HANDLE hHandle, INT nBlockN, LPCVOID pData4, INT nPar, UINT nPsw, UINT nFlags);
ZREADER_API(ZR_STATUS) ZR_Rd_ResetT57(HANDLE hHandle);

ZREADER_API(ZR_STATUS) ZR_DecodeT57Config(PZR_T57_CONFIG pConfig, LPCVOID pData4);
ZREADER_API(ZR_STATUS) ZR_EncodeT57Config(LPVOID pBuf4, PZR_T57_CONFIG pConfig);
ZREADER_API(ZR_STATUS) ZR_DecodeT57EmMarine(PINT pBitOffs, Z_KEYNUM* pNum, LPCVOID pData, INT nCount);
ZREADER_API(ZR_STATUS) ZR_EncodeT57EmMarine(LPVOID pBuf8_, INT nBufSize, INT nBitOffs, Z_KEYNUM* pNum);
ZREADER_API(ZR_STATUS) ZR_DecodeT57Hid(Z_KEYNUM* pNum, LPCVOID pData12, PINT pWiegand);
ZREADER_API(ZR_STATUS) ZR_EncodeT57Hid(LPVOID pBuf12, Z_KEYNUM* pNum, INT nWiegand);

ZREADER_API(ZR_STATUS) ZR_Rd_GetEncodedCardNumber(HANDLE hHandle, ZR_CARD_TYPE* pType, Z_KEYNUM* pNum, LPVOID pBuf, INT nBufSize, LPINT pRCount);

// Z2USB MF � Matrix-III Net

ZREADER_API(ZR_STATUS) ZR_Rd_EnumCards(HANDLE hHandle, ZR_ENUMCARDSPROC pEnumProc, PVOID pUserData);
ZREADER_API(ZR_STATUS) ZR_Rd_SelectCard(HANDLE hHandle, Z_KEYNUM* pNum);
ZREADER_API(ZR_STATUS) ZR_Rd_AuthorizeSect(HANDLE hHandle, INT nBlockN, BOOL fKeyB, ZR_MF_AUTH_KEY nKey);
ZREADER_API(ZR_STATUS) ZR_Rd_AuthorizeSectByEKey(HANDLE hHandle, INT nBlockN, BOOL fKeyB, UINT nKeyMask, PINT pRKeyIdx);
ZREADER_API(ZR_STATUS) ZR_Rd_ReadMfCardBlock(HANDLE hHandle, INT nBlockN, LPVOID pBuf16);
ZREADER_API(ZR_STATUS) ZR_Rd_WriteMfCardBlock(HANDLE hHandle, INT nBlockN, LPCVOID pData16);
ZREADER_API(ZR_STATUS) ZR_Rd_WriteMfCardBlock4(HANDLE hHandle, INT nBlockN, LPCVOID pData4);

ZREADER_API(ZR_STATUS) ZR_Rd_GetIndicatorState(HANDLE hHandle, LPDWORD pRed, LPDWORD pGreen, LPDWORD pSound);
ZREADER_API(ZR_STATUS) ZR_Rd_SetIndicatorState(HANDLE hHandle, ZR_IND_STATE nRed, ZR_IND_STATE nGreen, ZR_IND_STATE nSound);
ZREADER_API(ZR_STATUS) ZR_Rd_AddIndicatorFlash(HANDLE hHandle, PZR_IND_FLASH pRecs, INT nCount, BOOL fReset, PINT pRCount);
ZREADER_API(ZR_STATUS) ZR_Rd_BreakIndicatorFlash(HANDLE hHandle, BOOL fAutoMode);
ZREADER_API(ZR_STATUS) ZR_Rd_GetIndicatorFlashAvailable(HANDLE hHandle, PINT pCount);

// ������� ���������� (���������: �����������, �����������)

ZREADER_API(ZR_STATUS) ZR_Rd_Reset1356(HANDLE hHandle, WORD nWaitMs);
ZREADER_API(ZR_STATUS) ZR_Rd_PowerOff(HANDLE hHandle);

// ������� ISO

ZREADER_API(ZR_STATUS) ZR_Rd_Request(HANDLE hHandle, BOOL fWakeUp, ZR_CARD_TYPE* pType, PWORD pATQ);
ZREADER_API(ZR_STATUS) ZR_Rd_Halt(HANDLE hHandle);
ZREADER_API(ZR_STATUS) ZR_Rd_A_S(HANDLE hHandle, Z_KEYNUM* pNum, PBYTE pSAK);
ZREADER_API(ZR_STATUS) ZR_Rd_R_A_S(HANDLE hHandle, BOOL fWakeUp, ZR_CARD_TYPE* pType, Z_KEYNUM* pNum, PBYTE pSAK, PWORD pATQ);
ZREADER_API(ZR_STATUS) ZR_Rd_R_R(HANDLE hHandle, Z_KEYNUM* pNum, BOOL fWakeUp);
ZREADER_API(ZR_STATUS) ZR_Rd_Auth(HANDLE hHandle, UINT nAddr, BOOL fKeyB, PZR_MF_AUTH_KEY pKey, UINT nEKeyMask, PINT pEKeyIdx);
ZREADER_API(ZR_STATUS) ZR_Rd_Read16(HANDLE hHandle, UINT nAddr, LPVOID pBuf16);
ZREADER_API(ZR_STATUS) ZR_Rd_Write16(HANDLE hHandle, UINT nAddr, LPCVOID pData16);
// ������ ����� 4
ZREADER_API(ZR_STATUS) ZR_Rd_Write4(HANDLE hHandle, UINT nAddr, LPCVOID pData4);
ZREADER_API(ZR_STATUS) ZR_Rd_Increment(HANDLE hHandle, UINT nAddr, UINT nValue);
ZREADER_API(ZR_STATUS) ZR_Rd_Decrement(HANDLE hHandle, UINT nAddr, UINT nValue);
ZREADER_API(ZR_STATUS) ZR_Rd_Transfer(HANDLE hHandle, UINT nAddr);
ZREADER_API(ZR_STATUS) ZR_Rd_Restore(HANDLE hHandle, UINT nAddr);
// ������ ����� � EEPROM
ZREADER_API(ZR_STATUS) ZR_Rd_WriteKeyToEEPROM(HANDLE hHandle, UINT nAddr, BOOL fKeyB, PZR_MF_AUTH_KEY pKey);

ZREADER_API(ZR_STATUS) ZR_EncodeMfAccessBits(INT nAreaN, LPVOID pBuf3, UINT nBits);
ZREADER_API(ZR_STATUS) ZR_DecodeMfAccessBits(INT nAreaN, PUINT pBits, LPCVOID pData3);

// Matrix-III Net

// ��������� �������� �����
ZREADER_API(ZR_STATUS) ZR_M3n_ActivatePowerKey(HANDLE hHandle, BOOL fForce, UINT nTimeMs);
// ���������� �������� (���� ��������� �����������)
ZREADER_API(ZR_STATUS) ZR_M3n_SetOutputs(HANDLE hHandle, UINT nMask, UINT nOutBits);
// ������� �����
ZREADER_API(ZR_STATUS) ZR_M3n_GetInputs(HANDLE hHandle, PUINT pFlags);
// ���������� ���������
ZREADER_API(ZR_STATUS) ZR_M3n_SetConfig(HANDLE hHandle, PZR_M3N_CONFIG pConfig);
// ������� ���������
ZREADER_API(ZR_STATUS) ZR_M3n_GetConfig(HANDLE hHandle, PZR_M3N_CONFIG pConfig);
// ���������� ��������� security
ZREADER_API(ZR_STATUS) ZR_M3n_SetSecurity(HANDLE hHandle, INT nBlockN, UINT nKeyMask, BOOL fKeyB);
// �������� ��������� security
ZREADER_API(ZR_STATUS) ZR_M3n_GetSecurity(HANDLE hHandle, PINT pBlockN, PUINT pKeyMask, PBOOL pKeyB);
// ���������� ����
ZREADER_API(ZR_STATUS) ZR_M3n_SetClock(HANDLE hHandle, LPSYSTEMTIME pTime);
// �������� ����
ZREADER_API(ZR_STATUS) ZR_M3n_GetClock(HANDLE hHandle, LPSYSTEMTIME pTime);

// Z-2 Base
ZREADER_API(ZR_STATUS) ZR_Z2b_SetFormat(HANDLE hHandle, LPCWSTR pszFmt, LPCWSTR pszArg, LPCWSTR pszNoCard, BOOL fSaveEE);
ZREADER_API(ZR_STATUS) ZR_Z2b_GetFormat(HANDLE hHandle, LPWSTR szFmtBuf, INT nFmtBufSize, 
	LPWSTR szArgBuf, INT nArgBufSize, LPWSTR szNCBuf, INT nNCBufSize);
ZREADER_API(ZR_STATUS) ZR_Z2b_SetPowerState(HANDLE hHandle, BOOL fOn);

#endif // !ZREADER_LINKONREQUEST
