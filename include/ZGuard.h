#pragma once
// ����������� ���� ���� ifdef - ��� ����������� ����� �������� ��������, ���������� ��������� 
// �������� �� ��������� DLL. ��� ����� ������ DLL �������������� � �������������� ������� ZGUARD_EXPORTS,
// � ��������� ������. ���� ������ �� ������ ���� ��������� � �����-���� �������
// ������������ ������ DLL. ��������� ����� ����� ������ ������, ��� �������� ����� �������� ������ ����, ����� 
// ������� ZGUARD_API ��� ��������������� �� DLL, ����� ��� ������ DLL ����� �������,
// ������������ ������ ��������, ��� ����������������.

#ifndef ZGUARD_LINKONREQUEST

#ifdef ZGUARD_EXPORTS
#define ZGUARD_API(type) extern "C" __declspec(dllexport) type __stdcall 
#else
#ifdef ZGUARD_STATIC
#define ZGUARD_API(type) extern "C" type __stdcall 
#else
#define ZGUARD_API(type) extern "C" __declspec(dllimport) type __stdcall
#endif
#endif

#endif

// ����������� ������ SDK
#define ZG_SDK_VER_MAJOR	3
#define ZG_SDK_VER_MINOR	22

#include "ZPort.h"
#include "ZBase.h"

#pragma pack(1)   // turn byte alignment on

typedef INT ZG_STATUS;	// ��������� ���������� ������� DLL

#define ZG_SUCCESS						0		// �������� ��������� �������
#define ZG_E_CANCELLED					1		// �������� �������������
#define ZG_E_NOT_FOUND					2		// �� ������ (��� ������� ������)

#define ZG_E_INVALID_PARAM				-1		// ������������ ��������
#define ZG_E_OPEN_NOT_EXIST				-2		// ���� �� ����������
#define ZG_E_OPEN_ACCESS				-3		// ���� ����� ������ ����������
#define ZG_E_OPEN_PORT					-4		// ������ ������ �������� �����
#define ZG_E_PORT_IO_ERROR				-5		// ������ ����� (��������� �������� �� USB?)
#define ZG_E_PORT_SETUP					-6		// ������ ��������� �����
#define ZG_E_LOAD_FTD2XX				-7		// ��������� ��������� FTD2XX.DLL
#define ZG_E_INIT_SOCKET				-8		// �� ������� ���������������� ������
#define ZG_E_SERVERCLOSE				-9		// ���������� ������ �� ������� �������
#define ZG_E_NOT_ENOUGH_MEMORY			-10		// ������������ ������ ��� ��������� �������
#define ZG_E_UNSUPPORT					-11		// ������� �� ��������������
#define ZG_E_NOT_INITALIZED				-12		// �� ������������������� � ������� ZG_Initialize
#define ZG_E_CREATE_EVENT				-13		// ������ ������� CreateEvent

#define ZG_E_TOO_LARGE_MSG				-100	// ������� ������� ��������� ��� ��������
#define ZG_E_INSUFFICIENT_BUFFER		-101	// ������ ������ ������� ���
#define ZG_E_NO_ANSWER					-102	// ��� ������
#define ZG_E_BAD_ANSWER					-103	// �������������� �����
#define ZG_E_ONLY_GUARD					-104	// ������� �������� ������ � ����������� Guard
#define ZG_E_WRONG_ZPORT_VERSION		-107	// �� ���������� ������ ZPort.dll

// ������ ��� Guard
#define ZG_E_G_ONLY_ADVANCED			-200	// ������� �������� ������ � ����������� Guard � ������ Advanced
// ������, ������������ ����������� Guard
#define ZG_E_G_OTHER					-250	// ������ ������ ����������
#define ZG_E_G_LIC_OTHER				-251	// ������ ������ ��������
#define ZG_E_G_LIC_NOT_FOUND			-252	// ������ ����������: ��� ����� ��������
#define ZG_E_G_LIC_EXPIRED				-253	// ������� �������� �������
#define ZG_E_G_LIC_CTR_LIM				-254	// ������ ����������: ����������� �������� �� �����
#define ZG_E_G_LIC_RKEY_LIM				-255	// ����������� �������� �� ����� ������ ��� ������
#define ZG_E_G_LIC_WKEY_LIM				-256	// ����������� �������� �� ����� ������ ��� ������
#define ZG_E_G_LIC_EXPIRED2				-257	// ���� �������� ����� (���������� ��� ��������� ���� � �����������)
#define ZG_E_G_BAD_CS					-270	// ������ � ����������� ����� ������
#define ZG_E_G_CTR_NOT_FOUND			-271	// �������� ����� ����������� (���������� �� ������)
#define ZG_E_G_CMD_UNSUPPORT			-272	// ������� ����������������

#define ZG_E_CTR_NACK					-300	// ���������� ������� � ���������� �������
#define ZG_E_CTR_TRANSFER				-301	// ��������� �� ���� ��������� ������� �����������
#define ZG_E_BOOTLOADER_NOSTART			-302	// Bootloader not started
#define ZG_E_FIRMWARE_FILESIZE			-303	// Filesize does not match the request
#define ZG_E_FIRMWARE_NOSTART			-304	// Not found running firmware. Try restarting the device.

#define ZG_E_FW_NO_COMPATIBLE			-305	// Not compatible for this device
#define ZG_E_FW_INVALID_DEV_NUM			-306	// Not suitable for this device number
#define ZG_E_FW_TOOLARGE				-307	// Too large data firmware can be a mistake
#define ZG_E_FW_SEQUENCE_DATA			-308	// Violated the sequence data
#define ZG_E_FW_DATA_INTEGRITY			-309	// Compromise data integrity
#define ZG_E_FW_OTHER					-350	// ������ ������ ��� ������������

#define ZG_E_OTHER						-1000	// ������ ������


#define ZG_IF_ERROR_LOG					0x100	// ���������� � ���-���� ���������� �� ������� (%AppData%\RF Enabled\SDK Guard\errors.log)

#define ZG_DEVTYPE_GUARD				1
#define ZG_DEVTYPE_Z397					2

#define ZG_DEF_CVT_LICN					5	// ����� �������� ���������� �� ���������
#define ZG_MAX_TIMEZONES				7	// �������� ��������� ���


// ��� ����������
enum ZG_CVT_TYPE : INT
{
	ZG_CVT_UNDEF = 0,		// �� ����������
	ZG_CVT_Z397,			// Z-397
	ZG_CVT_Z397_GUARD,		// Z-397 Guard
	ZG_CVT_Z397_IP,			// Z-397 IP
	ZG_CVT_Z397_WEB,		// Z-397 Web
	ZG_CVT_Z5R_WEB			// Z5R Web
};

// ����� ���������� Z397 Guard
enum ZG_GUARD_MODE : INT
{
	ZG_GUARD_UNDEF = 0,		// �� ����������
	ZG_GUARD_NORMAL,		// ����� "Normal" (�������� �������� ���������� Z397)
	ZG_GUARD_ADVANCED,		// ����� "Advanced"
	ZG_GUARD_TEST,			// ����� "Test" (��� ������������)
	ZG_GUARD_ACCEPT			// ����� "Accept" (��� ������������)
};

// �������� ����������
enum ZG_CVT_SPEED : UINT
{
	ZG_SPEED_19200	= 19200,
	ZG_SPEED_57600	= 57600
};

// ���������� � ����������, ������������ �������� ZG_EnumConverters
typedef struct _ZG_ENUM_CVT_INFO : _ZP_DEVICE_INFO
{
	ZG_CVT_TYPE nType;		// ��� ����������
	ZG_GUARD_MODE nMode;	// ����� ������ ���������� Guard
} *PZG_ENUM_CVT_INFO;

typedef struct _ZG_ENUM_IPCVT_INFO : _ZP_DEVICE_INFO
{
	ZG_CVT_TYPE nType;		// ��� IP-����������
	ZG_GUARD_MODE nMode;	// ����� ������ ���������� Guard
	DWORD nFlags;			// �����: ��� 0 - "VCP", ��� 1 - "WEB", 0xFF - "All"
} *PZG_ENUM_IPCVT_INFO;

// ���������� � ����������, ������������ ���������: ZG_Cvt_Open, ZG_Cvt_AttachPort � ZG_Cvt_GetInformation
typedef struct _ZG_CVT_INFO : _ZP_DEVICE_INFO
{
	ZG_CVT_TYPE nType;		// ��� ����������
	ZG_CVT_SPEED nSpeed;	// �������� ����������

	ZG_GUARD_MODE nMode;	// ����� ������ ���������� Guard

	LPWSTR pszLinesBuf;		// ����� ��� �������������� �����
	INT nLinesBufMax;		// ������ ������ � ��������, ������� ����������� '\0'
} *PZG_CVT_INFO;

// ��������� �������� ���������� (��� ������� ZG_Cvt_Open)
typedef struct _ZG_CVT_OPEN_PARAMS
{
	ZP_PORT_TYPE nType;			// ��� �����
	LPCWSTR pszName;			// ��� �����. ���� =NULL, �� ������������ hPort
	HANDLE hPort;				// ���������� �����, ���������� �������� ZP_Open
	ZG_CVT_TYPE nCvtType;		// ��� ����������. ���� =ZG_CVT_UNDEF, �� ���������������
	ZG_CVT_SPEED nSpeed;		// �������� ����������
	PZP_WAIT_SETTINGS pWait;	// ��������� ��������. ����� ���� =NULL.
	BYTE nStopBits;				
	INT nLicN;					// ����� ��������. ���� =0, �� ������������ ZG_DEF_CVT_LICN
} *PZG_CVT_OPEN_PARAMS;

#define ZG_MAX_LICENSES		16	// ������������ ���������� ��������, ������� ����� ���������� � ���������

// ���������� � �������� ���������� Guard
typedef struct _ZG_CVT_LIC_INFO
{
	WORD nStatus;			// ������ ��������
	WORD Reserved;			// ��������������� ��� ������������ ���������
	INT nMaxCtrs;			// ������������ ���������� ������������
	INT nMaxKeys;			// ������������ ���������� ������
	WORD nMaxYear;			// ����: ��� (= 0xFFFF ���� ������������)
	WORD nMaxMon;			// ����: �����
	WORD nMaxDay;			// ����: ����
	WORD nDownCountTime;	// ���������� ����� ����� �������� � �������
} *PZG_CVT_LIC_INFO;

// ������� ���������� � �������� ���������� Guard
typedef struct _ZG_CVT_LIC_SINFO
{
	int nLicN;				// ����� ��������
	int nMaxCtrs;			// ������������ ���������� ������������
	INT nMaxKeys;			// ������������ ���������� ������
} *PZG_CVT_LIC_SINFO;

// ������ �����������
enum ZG_CTR_TYPE : INT
{
	ZG_CTR_UNDEF	= 0,	// �� ����������
	ZG_CTR_GATE2K,			// Gate 2000
	ZG_CTR_MATRIX2NET,		// Matrix II Net
	ZG_CTR_Z5RNET,			// Z5R Net
	ZG_CTR_Z5RNET8K,		// Z5R Net 8000
	ZG_CTR_GUARDNET,		// Guard Net
	ZG_CTR_Z9				// Z9
};

// ������ �����������
enum ZG_CTR_SUB_TYPE: INT
{
	ZG_CS_UNDEF	= 0,		// �� ����������
	ZG_CS_DOOR,				// �����
	ZG_CS_TURNSTILE,		// ��������
	ZG_CS_GATEWAY,			// ����
	ZG_CS_BARRIER			// ��������
};

// ����� �����������
#define ZG_CTR_F_2BANKS		1		// 2 ����� / 1 ����
#define ZG_CTR_F_PROXIMITY	2		// Proximity (Wiegand) / TouchMemory (Dallas)
#define ZG_CTR_F_JOIN		4		// ����������� ���� ������
#define ZG_CTR_F_X2			8		// �������� ������
#define ZG_CTR_F_ELECTRO	0x10	// ������� ElectroControl (��� Matrix II Net)

// ���������� � ��������� �����������, ������������ �������� ZG_Cvt_FindNextCtr
typedef struct _ZG_FIND_CTR_INFO
{
	ZG_CTR_TYPE nType;			// ��� �����������
	BYTE nTypeCode;				// ��� ���� �����������
	BYTE nAddr;					// ������� �����
	WORD nSn;					// ��������� �����
	WORD nVersion;				// ������ ��������
	INT nMaxKeys;				// �������� ������
	INT nMaxEvents;				// �������� �������
	UINT nFlags;				// ����� ����������� (ZG_CTR_F_...)
	ZG_CTR_SUB_TYPE nSubType;	// ������ �����������
} *PZG_FIND_CTR_INFO;

// ���������� � �����������, ������������ ���������: ZG_Ctr_Open � ZG_Ctr_GetInformation
typedef struct _ZG_CTR_INFO
{
	ZG_CTR_TYPE nType;			// ��� �����������
	BYTE nTypeCode;				// ��� ���� �����������
	BYTE nAddr;					// ������� �����
	WORD nSn;					// ��������� �����
	WORD nVersion;				// ������ ��
	INT nInfoLineCount;			// ���������� ����� � �����������
	INT nMaxKeys;				// �������� ������
	INT nMaxEvents;				// �������� �������
	UINT nFlags;				// ����� ����������� (ZG_CTR_F_...)
	WORD Reserved;				// ��������������� ��� ������������ ���������
	LPWSTR pszLinesBuf;			// ����� ��� �������������� �����
	INT nLinesBufMax;			// ������ ������ � ��������, ������� ����������� '\0'
	ZG_CTR_SUB_TYPE nSubType;	// ������ �����������
	INT nOptReadItems;			// ���������� ���������, ������� ����� ���� ������� ����� �������� ����������� 
	INT nOptWriteItems;			// ���������� ���������, ������� ����� ���� �������� ����� �������� �����������
} *PZG_CTR_INFO;

// ��������� ���� �����������
typedef struct _ZG_CTR_TIMEZONE
{
	BYTE nDayOfWeeks;		// ��� ������
	BYTE nBegHour;			// ������: ���
	BYTE nBegMinute;		// ������: ������
	BYTE nEndHour;			// �����: ���
	BYTE nEndMinute;		// �����: ������
	BYTE Reserved[3];
} *PZG_CTR_TIMEZONE;

// ��� ����� �����������
enum ZG_CTR_KEY_TYPE : INT
{
	ZG_KEY_UNDEF = 0,	// �� ����������
	ZG_KEY_NORMAL,		// �������
	ZG_KEY_BLOCKING,	// �����������
	ZG_KEY_MASTER		// ������
};

// ����� �����
#define ZG_KF_SHORT_NUM			1	// �������� �����. ���� fProximity=False, �� ���������� ����� ��������� ������ ������ 3 ����� ������ �����.
#define ZG_KF_ANTIPASSBACK		2	// ����������� ������������ (��� Gate 4000)

// ���� �����������
typedef struct _ZG_CTR_KEY
{
	BOOL fErased;				// TRUE, ���� ���� �����
	Z_KEYNUM rNum;				// ����� �����
	ZG_CTR_KEY_TYPE nType;		// ��� �����
	UINT nFlags;				// ����� ZG_KF_...
	UINT nAccess;				// ������ (����� ��������� ���)
	BYTE aData1[4];				// ������ ������ �����
} *PZG_CTR_KEY;

// ���� �����������
typedef struct _ZG_CTR_CLOCK
{
	// ���� "���� �����������"
	BOOL fStopped;				// TRUE, ���� ���� �����������
	// ���� � �����
	WORD nYear;					// ���
	WORD nMonth;				// �����
	WORD nDay;					// ����
	WORD nHour;					// ���
	WORD nMinute;				// ������
	WORD nSecond;				// �������
} *PZG_CTR_CLOCK;

// ��� ������� �����������
enum ZG_CTR_EV_TYPE : INT
{
	ZG_EV_UNKNOWN = 0,			// �� ����������
	ZG_EV_BUT_OPEN,				// ������� ������� �������
	ZG_EV_KEY_NOT_FOUND,		// ���� �� ������ � ����� ������
	ZG_EV_KEY_OPEN,				// ���� ������, ����� �������
	ZG_EV_KEY_ACCESS,			// ���� ������, ������ �� ��������
	ZG_EV_REMOTE_OPEN,			// ������� ���������� �� ����
	ZG_EV_KEY_DOOR_BLOCK,		// ���� ������, ����� �������������
	ZG_EV_BUT_DOOR_BLOCK,		// ������� ������� ��������������� ����� �������
	ZG_EV_NO_OPEN,				// ����� ��������
	ZG_EV_NO_CLOSE,				// ����� ��������� �������� (timeout)
	ZG_EV_PASSAGE,				// ������ ���������
	ZG_EV_SENSOR1,				// �������� ������ 1 (������)
	ZG_EV_SENSOR2,				// �������� ������ 2 (�����)
	ZG_EV_REBOOT,				// ������������ �����������
	ZG_EV_BUT_BLOCK,			// ������������� ������ ����������
	ZG_EV_DBL_PASSAGE,			// ������� �������� �������
	ZG_EV_OPEN,					// ����� ������� ������
	ZG_EV_CLOSE,				// ����� �������
	ZG_EV_POWEROFF,				// ������� �������
	ZG_EV_ELECTRO_ON,			// ��������� ��������������
	ZG_EV_ELECTRO_OFF,			// ���������� ��������������
	ZG_EV_LOCK_CONNECT,			// ��������� ����� (�������)
	ZG_EV_LOCK_DISCONNECT,		// ���������� ����� (�������)
	ZG_EV_FIRE_STATE,			// ��������� ��������� ������
	ZG_EV_SECUR_STATE,			// ��������� ��������� ������
	ZG_EV_UNKNOWN_KEY,			// ����������� ����
	ZG_EV_GATEWAY_PASS,			// �������� ���� � ����
	ZG_EV_GATEWAY_BLOCK,		// ������������ ���� � ���� (�����)
	ZG_EV_GATEWAY_ALLOWED,		// �������� ���� � ����
	ZG_EV_ANTIPASSBACK			// ������������ ������ (�����������)
};

// ������� �����������
typedef struct _ZG_CTR_EVENT
{
	ZG_CTR_EV_TYPE nType;			// ��� �������
	union
	{
		struct
		{
			BYTE nCode;				// ��� ������� � �����������
			BYTE aParams[7];		// ��������� �������
		} ep;
		BYTE aData[8];				// ������ ������� (����������� ������� �������������, �������������� ���� �������, 
									//	ZG_Ctr_DecodePassEvent, ZG_Ctr_DecodeEcEvent, ZG_Ctr_DecodeUnkKeyEvent, ZG_Ctr_DecodeFireEvent, ZG_Ctr_DecodeSecurEvent)
	};
} *PZG_CTR_EVENT;

// ����������� ������� �����������
enum ZG_CTR_DIRECT : INT
{
	ZG_DIRECT_UNDEF = 0,	// �� ����������
	ZG_DIRECT_IN,			// ����
	ZG_DIRECT_OUT			// �����
};

// ���� � ����� �������
typedef struct _ZG_EV_TIME
{
	BYTE nMonth;				// �����
	BYTE nDay;					// ����
	BYTE nHour;					// ���
	BYTE nMinute;				// ������
	BYTE nSecond;				// �������
	BYTE Reserved[3];
} *PZG_EV_TIME;

// �������, ��������� ������� ElectroControl: ZG_EV_ELECTRO_ON, ZG_EV_ELECTRO_OFF
enum ZG_EC_SUB_EV : INT
{
	ZG_EC_EV_UNDEF = 0,		// �� ����������
	ZG_EC_EV_CARD_DELAY,	// ��������� �������� ����� � ������ ������� (��� �����) �������� ��������
	ZG_EC_EV_RESERVED1,		// (���������������)
	ZG_EC_EV_ON_NET,		// �������� �������� �� ����
	ZG_EC_EV_OFF_NET,		// ��������� �������� �� ����
	ZG_EC_EV_ON_SCHED,		// �������� �� ��������� ����
	ZG_EC_EV_OFF_SHED,		// ��������� �� ��������� ����
	ZG_EC_EV_CARD,			// ��������� �������� ����� � ������������ ����������
	ZG_EC_EV_RESERVED2,		// (���������������)
	ZG_EC_EV_OFF_TIMEOUT,	// ��������� ����� ��������� ��������
	ZG_EC_EV_OFF_EXIT		// ��������� �� ������������ ������� ������
};

// �������, ��������� ������� ZG_EV_FIRE_STATE
enum ZG_FIRE_SUB_EV: INT
{
	ZG_FR_EV_UNDEF = 0,		// �� ����������
	ZG_FR_EV_OFF_NET,		// ��������� �� ����
	ZG_FR_EV_ON_NET,		// �������� �� ����
	ZG_FR_EV_OFF_INPUT_F,	// ��������� �� ����� FIRE
	ZG_FR_EV_ON_INPUT_F,	// �������� �� ����� FIRE
	ZG_FR_EV_OFF_TEMP,		// ��������� �� ������� �����������
	ZG_FR_EV_ON_TEMP		// �������� �� ������� �����������
};

// �������, ��������� ������� ZG_EV_SECUR_STATE
enum ZG_SECUR_SUB_EV: INT
{
	ZG_SR_EV_UNDEF = 0,		// �� ����������
	ZG_SR_EV_OFF_NET,		// ��������� �� ����
	ZG_SR_EV_ON_NET,		// �������� �� ����
	ZG_SR_EV_OFF_INPUT_A,	// ��������� �� ����� ALARM
	ZG_SR_EV_ON_INPUT_A,	// �������� �� ����� ALARM
	ZG_FR_EV_OFF_TAMPERE,	// ��������� �� �������
	ZG_FR_EV_ON_TAMPERE,	// �������� �� �������
	ZG_FR_EV_OFF_DOOR,		// ��������� �� ������� �����
	ZG_FR_EV_ON_DOOR		// �������� �� ������� �����
};


// ������ ������� ��������� ��� ������� ZG_Ctr_ControlDevices
#define ZG_DEV_RELE1		0	// ���� ����� 1
#define ZG_DEV_RELE2		1	// ���� ����� 2
#define ZG_DEV_SW3			2	// ������� ���� SW3 (��) ����.5 ������� �5
#define ZG_DEV_SW4			3	// ������� ���� SW4 (��) ����.5 ������� �6
#define ZG_DEV_SW0			4	// ������� ���� SW0 (��) ����.1 ������� �4
#define ZG_DEV_SW1			5	// ������� ���� SW1 (��) ����.3 ������� �4
#define ZG_DEV_K65			6	// ����������� ���� (��) ����.6 ������� �5
#define ZG_DEV_K66			7	// ����������� ���� (��) ����.6 ������� �6

// ����� ������������ ��������������
#define ZG_EC_CF_ENABLED		0x01	// ������������� ���������� ��������
#define ZG_EC_CF_SCHEDULE		0x02	// ������������ ��������� ���� 6 ��� ��������� �������
#define ZG_EC_CF_EXT_READER		0x04	// ����������� �����������: �0� Matrix-II Net, �1� ������� �����������
#define ZG_EC_CF_INVERT			0x08	// ������������� ����������� �����
#define ZG_EC_CF_EXIT_OFF		0x10	// ������������� ������ �����
#define ZG_EC_CF_CARD_OPEN		0x20	// �� ����������� ������� ���������� ��� ������������ �����������

// ������������ ���������� ���������������
typedef struct _ZG_CTR_ELECTRO_CONFIG
{
	DWORD nPowerConfig;		// ������������ ���������� ��������
	DWORD nPowerDelay;		// ����� �������� � ��������
	_ZG_CTR_TIMEZONE rTz6;	// ��������� ���� �6 (������� �� 0)
} *PZG_CTR_ELECTRO_CONFIG;

// ����� ��������� ��������������
#define ZG_EC_SF_ENABLED		0x01	// ��������� ������� � 1 ���/0 ����
#define ZG_EC_SF_SCHEDULE		0x02	// ������� ��������� �� ��������� ����
#define ZG_EC_SF_REMOTE			0x04	// �������� �� ������� �� ����
#define ZG_EC_SF_DELAY			0x08	// ���� ��������� ��������
#define ZG_EC_SF_CARD			0x10	// ����� � ���� ������������ �����������

// ��������� ��������������
typedef struct _ZG_CTR_ELECTRO_STATE
{
	DWORD nPowerFlags;		// ����� ��������� ��������������
	DWORD nPowerConfig;		// ������������ ���������� ��������
	DWORD nPowerDelay;		// ����� �������� � ��������
} *PZG_CTR_ELECTRO_STATE;

// ����� ��������� ������ �����
#define ZG_FR_F_ENABLED			0x01	// ��������� ��������� ������ � 1 ���/0 ����
#define ZG_FR_F_INPUT_F			0x02	// ������� �������� ����� �� ����� FIRE
#define ZG_FR_F_TEMP			0x04	// ������� �������� ����� �� ���������� �����������
#define ZG_FR_F_NET				0x08	// ������� �������� ����� �� ������� �������
// ����� ��� ����� ���������� ���������� ������ �����
#define ZG_FR_SRCF_INPUT_F		0x01	// �������� �������� ����� �� ����� FIRE
#define ZG_FR_SRCF_TEMP			0x02	// �������� �������� ����� �� ���������� �����������

// ����� ������
enum ZG_SECUR_MODE: INT
{
	ZG_SR_M_UNDEF = 0,		// �� ����������
	ZG_SR_M_SECUR_OFF,		// ��������� ����� ������
	ZG_SR_M_SECUR_ON,		// �������� ����� ������
	ZG_SR_M_ALARM_OFF,		// ��������� �������
	ZG_SR_M_ALARM_ON		// �������� �������
};

// ����� ��������� ������ ������
#define ZG_SR_F_ENABLED			0x01	// ��������� ��������� ������ � 1 ���/0 ����
#define ZG_SR_F_ALARM			0x02	// ��������� �������
#define ZG_SR_F_INPUT_A			0x04	// ������� �� ����� ALARM
#define ZG_SR_F_TAMPERE			0x08	// ������� �� �������
#define ZG_SR_F_DOOR			0x10	// ������� �� ������� �����
#define ZG_SR_F_NET				0x20	// ������� �������� �� ����
// ����� ��� ����� ���������� ���������� ������ ������
#define ZG_SR_SRCF_INPUT_F		0x01	// ��������� ������� �� ����� FIRE
#define ZG_SR_SRCF_TAMPERE		0x02	// ��������� ������� �� �������
#define ZG_SR_SRCF_DOOR			0x04	// ��������� ������� �� ������� �����

// ����� ��� _ZG_CVT_NOTIFY_SETTINGS.nNMask
#define ZG_NF_CVT_CTR_EXIST			0x01	// ZG_N_CVT_CTR_INSERT / ZG_N_CVT_CTR_REMOVE
#define ZG_NF_CVT_CTR_CHANGE		0x02	// ��������� ���������� ����������� ZG_N_CVT_CTR_CHANGE
#define ZG_NF_CVT_CTR_DBL_CHECK		0x1000	// ������ ��������� ���������� ������������
#define ZG_NF_CVT_REASSIGN_ADDRS	0x2000	// �������������� �������������� ������� ������������ (����� Guard Advanced) (�������� ������ � ZG_NF_CVT_CTR_EXIST)
#define ZG_NF_CVT_WND_SYNC			0x4000	// ���������������� � �������� ��������� Windows
#define ZG_NF_CVT_ONLY_NOTIFY		0x8000	// ������ ���������� � ���������� ����� ��������� � �������
#define ZG_NF_CVT_RESCAN_CTRS		0x10000	// ������ ������ ������������ ������������ (��� Z-397 � Z-397 Guard � ������ Normal)

#define ZG_N_CVT_CTR_INSERT			1	// ���������� ��������� PZG_FIND_CTR_INFO(MsgParam) - ���������� � �����������
#define ZG_N_CVT_CTR_REMOVE			2	// ���������� �������� PZG_FIND_CTR_INFO(MsgParam) - ���������� � �����������
#define ZG_N_CVT_CTR_CHANGE			3	// �������� ��������� ����������� PZG_N_CTR_CHANGE_INFO(MsgParam)

typedef struct _ZG_N_CTR_CHANGE_INFO
{
	UINT nChangeMask;			// ����� ��������� (���0 addr, ���1 version, ���2 flags)
	_ZG_FIND_CTR_INFO rCtrInfo;	// ���������� ���������� � �����������
	WORD nOldVersion;			// ������ �������� ������
	BYTE nOldAddr;				// ������ �������� ������
	BYTE Reserved;				// ��������������� ��� ������������ ���������
} *PZG_N_CTR_CHANGE_INFO;

// ��������� ��� ����������� �� ����������
typedef struct _ZG_CVT_NOTIFY_SETTINGS
{
	UINT nNMask;				// ����� ����� ����������� (ZG_NF_CVT_...)

	ZP_NOTIFYPROC pfnCallback;	// Callback-�������
	PVOID pUserData;			// �������� ��� Callback-�������

	DWORD nScanCtrsPeriod;		// ������ ������������ ������ ������������ � �� (=0 ������������ �������� �� ���������, 5000)
	INT nScanCtrsLastAddr;		// ��������� ����������� ����� ����������� (��� Z-397)
} *PZG_CVT_NOTIFY_SETTINGS;

// ����� ��� _ZG_CTR_NOTIFY_SETTINGS.nNMask
#define ZG_NF_CTR_NEW_EVENT			0x01	// ZG_N_CTR_NEW_EVENT
#define ZG_NF_CTR_CLOCK				0x02	// ZG_N_CTR_CLOCK
#define ZG_NF_CTR_KEY_TOP			0x04	// ZG_N_CTR_KEY_TOP
#define ZG_NF_CTR_ADDR_CHANGE		0x08	// ZG_N_CTR_ADDR_CHANGE
#define ZG_NF_CTR_WND_SYNC			0x4000	// ���������������� � �������� ��������� Windows
#define ZG_NF_CTR_ONLY_NOTIFY		0x8000	// ������ ���������� � ���������� ����� ��������� � �������

// ����������� ������� ZG_Ctr_FindNotification

typedef struct _ZG_N_NEW_EVENT_INFO
{
	INT nNewCount;		// ���������� ����� �������
	INT nWriteIdx;		// ��������� ������
	INT nReadIdx;		// ��������� ������
	Z_KEYNUM rLastNum;	// ����� ���������� ������������ �����
} *PZG_N_NEW_EVENT_INFO;

#define ZG_N_CTR_NEW_EVENT			1	// ����� ������� PZG_N_NEW_EVENT_INFO(MsgParam) - ����������

#define ZG_N_CTR_CLOCK				2	// �������� ���������������� � �������� PINT64(MsgParam)


typedef struct _ZG_N_KEY_TOP_INFO
{
	INT nBankN;			// ����� ����� ������
	INT nNewTopIdx;		// ����� �������� ������� ������� ������
	INT nOldTopIdx;		// ������ �������� ������� ������� ������
} *PZG_N_KEY_TOP_INFO;

#define ZG_N_CTR_KEY_TOP			3	// ���������� ������� ������� ������ PZG_N_KEY_TOP_INFO(MsgParam) - ����������

#define ZG_N_CTR_ADDR_CHANGE		4	// ������� ������� ����� ����������� MsgParam = NewAddr

// ��������� ��� ����������� �� �����������
typedef struct _ZG_CTR_NOTIFY_SETTINGS
{
	UINT nNMask;				// ����� ����� ����������� (ZG_NF_CTR_...)

	ZP_NOTIFYPROC pfnCallback;	// Callback-�������
	PVOID pUserData;			// �������� ��� Callback-�������

	INT nReadEvIdx;				// ��������� ������ �������
	DWORD nCheckStatePeriod;	// ������ �������� ��������� ����������� (����� �������, �����, ������� ������� ������) � �� (=0 ������������ �������� �� ���������, 1000)

	DWORD nClockOffs;			// �������� ����� ����������� �� ����� �� � ��������
} *PZG_CTR_NOTIFY_SETTINGS;

typedef BOOL (CALLBACK* ZG_ENUMCVTSPROC) (PZG_ENUM_CVT_INFO pInfo, PZP_PORT_INFO pPort, PVOID pUserData);
typedef BOOL (CALLBACK* ZG_ENUMIPCVTSPROC) (PZG_ENUM_IPCVT_INFO pInfo, PZP_PORT_INFO pPort, PVOID pUserData);
typedef BOOL (CALLBACK* ZG_PROCESSCALLBACK) (INT nPos, INT nMax, PVOID pUserData);
typedef BOOL (CALLBACK* ZG_ENUMCTRSPROC) (PZG_FIND_CTR_INFO pInfo, INT nPos, INT nMax, PVOID pUserData);
typedef BOOL (CALLBACK* ZG_ENUMCTRTIMEZONESPROC) (INT nIdx, PZG_CTR_TIMEZONE pTz, PVOID pUserData);
typedef BOOL (CALLBACK* ZG_ENUMCTRKEYSPROC) (INT nIdx, PZG_CTR_KEY pKey, INT nPos, INT nMax, PVOID pUserData);
typedef BOOL (CALLBACK* ZG_ENUMCTREVENTSPROC) (INT nIdx, PZG_CTR_EVENT pEvent, INT nPos, INT nMax, PVOID pUserData);

#pragma pack() // turn byte alignment off

#ifndef ZGUARD_LINKONREQUEST

// ���������� ������ ���������� ZGuard.dll
ZGUARD_API(DWORD) ZG_GetVersion();

// �������������/����������� ����������
ZGUARD_API(ZG_STATUS) ZG_Initialize(UINT nFlags);
ZGUARD_API(ZG_STATUS) ZG_Finalyze();

//////////////////////////////////////////////////////////////////////////
// ������� ��� ������ � �����������
//

#ifndef ZGUARD_EXPORTS
#define ZG_EnumSerialPorts(pEnumProc, pUserData) (ZG_STATUS)ZP_EnumSerialPorts(6, pEnumProc, pUserData)
// ����������� ��� ������������ ����������
inline ZG_STATUS ZG_EnumConverters(PZP_PORT_ADDR pPorts, INT nPCount, 
		ZG_ENUMCVTSPROC pEnumProc, PVOID pUserData, PZP_WAIT_SETTINGS pWait=NULL)
	{ return (ZG_STATUS)ZP_EnumSerialDevices(6, pPorts, nPCount, (ZP_ENUMDEVICEPROC)pEnumProc, pUserData, pWait); };
inline ZG_STATUS ZG_EnumIpConverters(ZG_ENUMIPCVTSPROC pEnumProc, PVOID pUserData, PZP_WAIT_SETTINGS pWait=NULL)
	{ return (ZG_STATUS)ZP_EnumIpDevices(2, (ZP_ENUMDEVICEPROC)pEnumProc, pUserData, pWait); }
inline ZG_STATUS ZG_FindConverter(PZP_PORT_ADDR pPorts, INT nPCount,  
		PZG_ENUM_CVT_INFO pInfo, PZP_PORT_INFO pPort, PZP_WAIT_SETTINGS pWait=NULL)
	{ return (ZG_STATUS)ZP_FindSerialDevice(6, pPorts, nPCount, pInfo, sizeof(_ZG_ENUM_CVT_INFO), pPort, pWait); };
// ����������� �����������
inline ZG_STATUS ZG_FindNotification(PHANDLE pHandle, PZP_NOTIFY_SETTINGS pSettings, BOOL fSerial, BOOL fIP)
{
	if (fSerial)
		pSettings->nSDevTypes |= 6; 
	if (fIP)
		pSettings->nIpDevTypes |= 2;
	return (ZG_STATUS)ZP_FindNotification(pHandle, pSettings); 
}
#define ZG_CloseNotification(hHandle)	(ZG_STATUS)ZP_CloseNotification(hHandle)
#define ZG_ProcessMessages(hHandle, pEnumProc, pUserData)	(ZG_STATUS)ZP_ProcessMessages(hHandle, pEnumProc, pUserData)
#endif

// ��������� ����� �������� � ���������
ZGUARD_API(ZG_STATUS) ZG_UpdateCvtFirmware(PZG_CVT_OPEN_PARAMS pParams, 
	LPCVOID pData, INT nCount, ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData);

// ���������/��������� ���������
ZGUARD_API(ZG_STATUS) ZG_Cvt_Open(PHANDLE pHandle, PZG_CVT_OPEN_PARAMS pParams, PZG_CVT_INFO pInfo = NULL);
ZGUARD_API(ZG_STATUS) ZG_Cvt_Close(HANDLE hHandle);
// ����������� �� ����������, �� �������� ����, ���������� ���������� �����, ���������� �������� ZP_Open
ZGUARD_API(ZG_STATUS) ZG_Cvt_DettachPort(HANDLE hHandle, PHANDLE pPortHandle);

// ����������/������������� ��������� �������� ���������� �������
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetWaitSettings(HANDLE hHandle, PZP_WAIT_SETTINGS pSetting);
ZGUARD_API(ZG_STATUS) ZG_Cvt_SetWaitSettings(HANDLE hHandle, PZP_WAIT_SETTINGS pSetting);

// ��������� ���� (���������������� ������� �������� � ������)
ZGUARD_API(ZG_STATUS) ZG_Cvt_SetCapture(HANDLE hHandle);
// ��������� ���� (������������ ������� �������� � ������)
ZGUARD_API(ZG_STATUS) ZG_Cvt_ReleaseCapture(HANDLE hHandle);
// ������� ���� ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_Clear(HANDLE hHandle);
// ���������� ������ ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_Send(HANDLE hHandle, LPCVOID pData, INT nCount);
// ���������� ����� �� ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_Receive(HANDLE hHandle, LPVOID pBuf, INT nBufSize, LPINT pCount);
// ���������� ������ � ���������� ����� �� ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_Exec(HANDLE hHandle, LPCVOID pData, INT nCount, 
	LPVOID pBuf, INT nBufSize, LPINT pRCount);

// ����������� ��� ������������ ����������� (��� �� Guard-����������� ����� �������������� ��������� ����������� � �����/����. ������������)
ZGUARD_API(ZG_STATUS) ZG_Cvt_EnumControllers(HANDLE hHandle, ZG_ENUMCTRSPROC pEnumProc, PVOID pUserData, BOOL fUpdate=TRUE);
// ����� ����������� �� �������� ������
ZGUARD_API(ZG_STATUS) ZG_Cvt_FindController(HANDLE hHandle, BYTE nAddr, PZG_FIND_CTR_INFO pInfo);

// ���������� ���������� � ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetInformation(HANDLE hHandle, PZG_CVT_INFO pInfo);

// ����������� ����������� �� ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_FindNotification(HANDLE hHandle, PZG_CVT_NOTIFY_SETTINGS pSettings);
ZGUARD_API(ZG_STATUS) ZG_Cvt_ProcessMessages(HANDLE hHandle, ZP_NOTIFYPROC pEnumProc, PVOID pUserData);
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetScanCtrsState(HANDLE hHandle, LPINT pNextAddr);

// ��������� � ��������� ����� ��������
ZGUARD_API(ZG_STATUS) ZG_Cvt_UpdateFirmware(HANDLE hHandle, LPCVOID pData, INT nCount, 
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData);

// ���������� ���������� � �������� ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetLicense(HANDLE hHandle, BYTE nLicN, PZG_CVT_LIC_INFO pInfo);
// ������������� ����� ��������
ZGUARD_API(ZG_STATUS) ZG_Cvt_SetLicenseData(HANDLE hHandle, BYTE nLicN, LPCVOID pData, INT nCount, LPWORD pLicStatus);
// ������� ��� �������� � ����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_ClearAllLicenses(HANDLE hHandle);
// ���������� ���������� � ���� ���������, ������������� � ���������
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetAllLicenses(HANDLE hHandle, PZG_CVT_LIC_SINFO pBuf, INT nBufSize, LPINT pCount);

// ���������� �/� ���������� � ����� (������ Guard)
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetShortInfo(HANDLE hHandle, LPWORD pSn, ZG_GUARD_MODE* pMode);
// ���������� �������������� ������ ���������� (������ Guard)
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetLongInfo(HANDLE hHandle, PWORD pSn, PWORD pVersion, ZG_GUARD_MODE* pMode,
	LPSTR pBuf, INT nBufSize, LPINT pLen); // �������� 'i'

// ��������� � ���������� ����� ��������
ZGUARD_API(ZG_STATUS) ZG_Cvt_UpdateCtrFirmware(HANDLE hHandle, WORD nCtrSn, LPCVOID pData, INT nCount, LPCSTR pszInfoStr, 
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData);

// ���������� ����� ������� ����� ����������� �� �/� (� Advanced ������ �� ��������)
ZGUARD_API(ZG_STATUS) ZG_Cvt_SetCtrAddrBySn(HANDLE hHandle, WORD nSn, BYTE nNewAddr);
ZGUARD_API(ZG_STATUS) ZG_Cvt_SetCtrAddr(HANDLE hHandle, BYTE nOldAddr, BYTE nNewAddr);

// ����������� ���������� � ����������� �� �������� ������
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetCtrInfoNorm(HANDLE hHandle, BYTE nAddr, PBYTE pTypeCode, LPWORD pSn, LPWORD pVersion, LPINT pInfoLines, PUINT pFlags);
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetCtrInfoAdv(HANDLE hHandle, BYTE nAddr, PBYTE pTypeCode, LPWORD pSn, LPWORD pVersion, PUINT pFlags, PINT pEvWrIdx, PINT pEvRdIdx);
// ����������� ���������� � ����������� �� ��������� ������
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetCtrInfoBySn(HANDLE hHandle, WORD nSn, PBYTE pTypeCode, LPBYTE pAddr, LPWORD pVersion, LPINT pInfoLines, PUINT pFlags);
// ���������� �������������� ������ �����������
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetCtrInfoLine(HANDLE hHandle, WORD nSn, INT nLineN, LPSTR pBuf, INT nBufSize, LPINT pLen=NULL);
// ���������� ���������� � ������ ����������� (����� ������������ ��� �������� ����������� �����������)
ZGUARD_API(ZG_STATUS) ZG_Cvt_GetCtrVersion(HANDLE hHandle, BYTE nAddr, LPBYTE pVerData5, DWORD nTimeOut=0);

//////////////////////////////////////////////////////////////////////////
// ������� ��� ������ � ������������
//

// ���������/��������� ����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_Open(PHANDLE pHandle, HANDLE hCvtHandle, BYTE nAddr, WORD nSn, PZG_CTR_INFO pInfo = NULL);
ZGUARD_API(ZG_STATUS) ZG_Ctr_Close(HANDLE hHandle);
// ���������� ���������� � �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_GetInformation(HANDLE hHandle, PZG_CTR_INFO pInfo);
// ����������� ����������� �� �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_FindNotification(HANDLE hHandle, PZG_CTR_NOTIFY_SETTINGS pSettings);
ZGUARD_API(ZG_STATUS) ZG_Ctr_ProcessMessages(HANDLE hHandle, ZP_NOTIFYPROC pEnumProc, PVOID pUserData);
// ������������� ������� ����� ����������� (Guard �� ������������)
ZGUARD_API(ZG_STATUS) ZG_Ctr_SetNewAddr(HANDLE hHandle, BYTE nNewAddr);
// ��������� ���������� ����������� � ����� ������� ������� �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_AssignAddr(HANDLE hHandle, BYTE nAddr);
// ��������� ����� �������� � ����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_UpdateFirmware(HANDLE hHandle, LPCVOID pData, INT nCount, LPCSTR pszInfoStr, 
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData);

// ������� �����
ZGUARD_API(ZG_STATUS) ZG_Ctr_OpenLock(HANDLE hHandle, INT nLockN=0);
// ���������� �����
ZGUARD_API(ZG_STATUS) ZG_Ctr_CloseLock(HANDLE hHandle);
// �������� ����� ���������� ���������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_EnableEmergencyUnlocking(HANDLE hHandle, BOOL fEnable);
// ���������� ��������� ������ ���������� ���������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_IsEmergencyUnlockingEnabled(HANDLE hHandle, PBOOL pEnabled);
// ���������� �����
ZGUARD_API(ZG_STATUS) ZG_Ctr_Reset(HANDLE hHandle);
// ���������� ���������� ������ FLASH
ZGUARD_API(ZG_STATUS) ZG_Ctr_UpdateFlash(HANDLE hHandle);
// ���������� ������ �����
ZGUARD_API(ZG_STATUS) ZG_Ctr_HardReset(HANDLE hHandle);

// ������� �������� �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadRegs(HANDLE hHandle, DWORD nAddr, INT nCount, LPVOID pBuf);
// ������� ��������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadPorts(HANDLE hHandle, LPDWORD pData);
// ���������� �������� ������������ (nDevType - ����� ���������� ZG_DEV_..)
ZGUARD_API(ZG_STATUS) ZG_Ctr_ControlDevices(HANDLE hHandle, DWORD nDevType, BOOL fActive, DWORD nTimeMs=0);

// ������ / ������ ������ ����������� (nBankN - ����� �� ����: =0 ����, =1 �����, =2 �������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadData(HANDLE hHandle, INT nBankN, DWORD nAddr, INT nCount, LPVOID pBuf, 
	LPINT pReaded, ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData);
ZGUARD_API(ZG_STATUS) ZG_Ctr_WriteData(HANDLE hHandle, INT nBankN, DWORD nAddr, LPCVOID pData, INT nCount, 
	LPINT pWritten, ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData);

// ���������� ������� ��� ������ �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadLockTimes(HANDLE hHandle, LPDWORD pOpenMs, LPDWORD pLetMs, LPDWORD pMaxMs, INT nBankN=0);
// ������������� ������� ��� ������ �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_WriteLockTimes(HANDLE hHandle, DWORD nMask, DWORD nOpenMs, DWORD nLetMs, DWORD nMaxMs, INT nBankN=0);

// ���������� ���� ��� ��������� ��������� ���
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadTimeZones(HANDLE hHandle, INT nIdx, PZG_CTR_TIMEZONE pBuf, INT nCount, 
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData, INT nBankN=0);
// ������������� ���� ��� ��������� ��������� ���
ZGUARD_API(ZG_STATUS) ZG_Ctr_WriteTimeZones(HANDLE hHandle, INT nIdx, PZG_CTR_TIMEZONE pTzs, INT nCount,
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData, INT nBankN=0);
// ����������� ��������� ���� � �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_EnumTimeZones(HANDLE hHandle, INT nStart, ZG_ENUMCTRTIMEZONESPROC fnEnumProc, PVOID pUserData, INT nBankN=0);

// ���������� ���� ��� ��������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadKeys(HANDLE hHandle, INT nIdx, PZG_CTR_KEY pBuf, INT nCount,
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData, INT nBankN=0);
// ������������� ���� ��� ��������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_WriteKeys(HANDLE hHandle, INT nIdx, PZG_CTR_KEY pKeys, INT nCount,
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData, INT nBankN=0, BOOL fUpdateTop=TRUE);
// ������� ���� ��� ��������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ClearKeys(HANDLE hHandle, INT nIdx, INT nCount, 
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData, INT nBankN=0, BOOL fUpdateTop=TRUE);
// ���������� ������ ������� ������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_GetKeyTopIndex(HANDLE hHandle, LPINT pIdx, INT nBankN=0);
// ����������� ����� � �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_EnumKeys(HANDLE hHandle, INT nStart, ZG_ENUMCTRKEYSPROC fnEnumProc, PVOID pUserData, INT nBankN=0);

// ���������� ���� �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_GetClock(HANDLE hHandle, PZG_CTR_CLOCK pClock);
// ������������� ���� �����������
ZGUARD_API(ZG_STATUS) ZG_Ctr_SetClock(HANDLE hHandle, PZG_CTR_CLOCK pClock);

// ���������� ����� ���������� ������������ �����
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadLastKeyNum(HANDLE hHandle, Z_KEYNUM* pNum);
// ���������� ��������� ������� � ����� ���������� ������������ ����� (���� *pWrIdx ��� *pRdIdx == -1, �� ��������� ������������)
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadRTCState(HANDLE hHandle, PZG_CTR_CLOCK pClock, 
	LPINT pWrIdx, LPINT pRdIdx, Z_KEYNUM* pNum);
// ���������� ��������� �������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadEventIdxs(HANDLE hHandle, LPINT pWrIdx, LPINT pRdIdx);
// ������������� ��������� �������
ZGUARD_API(ZG_STATUS) ZG_Ctr_WriteEventIdxs(HANDLE hHandle, UINT nMask, INT nWrIdx, INT nRdIdx);
// ���������� ���� ��� ��������� �������
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadEvents(HANDLE hHandle, INT nIdx, PZG_CTR_EVENT pBuf, INT nCount,
	ZG_PROCESSCALLBACK pfnCallback, PVOID pUserData);
// ����������� ������� �����������
// nStart - ������ ������� �������, ���� =-1, ������������ ��������� ������,
// nCount - ���������� ������������� �������, ���� =-1, �� ������������ ���������� ������� � nStart �� ��������� ������,
//			���� =MAXINT, �� ������������� ��� �������
ZGUARD_API(ZG_STATUS) ZG_Ctr_EnumEvents(HANDLE hHandle, INT nStart, INT nCount,
	ZG_ENUMCTREVENTSPROC fnEnumProc, PVOID pUserData);
// ������������� ������� �������
ZGUARD_API(ZG_STATUS) ZG_Ctr_DecodePassEvent(HANDLE hHandle, PBYTE pData8, 
	PZG_EV_TIME pTime, ZG_CTR_DIRECT* pDirect, PINT pKeyIdx, PINT pKeyBank);
// ������������� ������� ElectoControl: ZG_EV_ELECTRO_ON, ZG_EV_ELECTRO_OFF
ZGUARD_API(ZG_STATUS) ZG_Ctr_DecodeEcEvent(HANDLE hHandle, PBYTE pData8, 
	PZG_EV_TIME pTime, ZG_EC_SUB_EV* pSubEvent, PDWORD pPowerFlags);
// ������������� ������� "����������� ����": ZG_EV_UNKNOWN_KEY
ZGUARD_API(ZG_STATUS) ZG_Ctr_DecodeUnkKeyEvent(HANDLE hHandle, PBYTE pData8, PZ_KEYNUM pKeyNum);
// ������������� ������� "��������� ��������� ������": ZG_EV_FIRE_STATE
ZGUARD_API(ZG_STATUS) ZG_Ctr_DecodeFireEvent(HANDLE hHandle, PBYTE pData8, 
	PZG_EV_TIME pTime, ZG_FIRE_SUB_EV* pSubEvent, PDWORD pFireFlags);
// ������������� ������� "��������� ��������� ������": ZG_EV_SECUR_STATE
ZGUARD_API(ZG_STATUS) ZG_Ctr_DecodeSecurEvent(HANDLE hHandle, PBYTE pData8, 
	PZG_EV_TIME pTime, ZG_SECUR_SUB_EV* pSubEvent, PDWORD pSecurFlags);

// ���������� �������� ������� �� ����
ZGUARD_API(ZG_STATUS) ZG_Ctr_SetFireMode(HANDLE hHandle, BOOL fOn);
// ������ ��������� ��������� ������
// pFireFlags - ����� ��������� ZG_FR_F_...
// pCurrTemp - ������� ����������� (� ��������)
// pSrcMask - ����� ����������� ���������� ZG_FR_SRCF_...
// pLimitTemp - ��������� ����������� (� ��������)
ZGUARD_API(ZG_STATUS) ZG_Ctr_GetFireInfo(HANDLE hHandle, PDWORD pFireFlags, 
	PDWORD pCurrTemp, PDWORD pSrcMask, PDWORD pLimitTemp);
// ��������� ���������� ��������� ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_SetFireConfig(HANDLE hHandle, DWORD nSrcMask, DWORD nLimitTemp, 
	PDWORD pFireFlags, PDWORD pCurrTemp);

// ���������� ������� ������ �� ����
ZGUARD_API(ZG_STATUS) ZG_Ctr_SetSecurMode(HANDLE hHandle, ZG_SECUR_MODE nMode);
// ������ ��������� ������ ������
// pSecurFlags - ����� ��������� ZG_SR_F_
// pSrcMask - ����� ����������� ���������� ZG_SR_SRCF_...
// pAlarmTime - ����� �������� ������ (� ��������)
ZGUARD_API(ZG_STATUS) ZG_Ctr_GetSecurInfo(HANDLE hHandle, PDWORD pSecurFlags, 
	PDWORD pSrcMask, PDWORD pAlarmTime);
// ��������� ���������� ������ ������
ZGUARD_API(ZG_STATUS) ZG_Ctr_SetSecurConfig(HANDLE hHandle, DWORD nSrcMask, DWORD nAlarmTime, 
	PDWORD pSecurFlags=NULL);

// ��� ����.������� Matrix II Net
// ���������� ��������� �������������� (��.���� � �������� 6)
ZGUARD_API(ZG_STATUS) ZG_Ctr_ReadElectroConfig(HANDLE hHandle, PZG_CTR_ELECTRO_CONFIG pConfig);
// ������������� ����� ��������� �������������� (��.���� � �������� 6)
ZGUARD_API(ZG_STATUS) ZG_Ctr_WriteElectroConfig(HANDLE hHandle, PZG_CTR_ELECTRO_CONFIG pConfig, BOOL fSetTz=TRUE);
// ���������� ������ ��������������
ZGUARD_API(ZG_STATUS) ZG_Ctr_GetElectroState(HANDLE hHandle, PZG_CTR_ELECTRO_STATE pState);
// ���./����. ��������������
ZGUARD_API(ZG_STATUS) ZG_Ctr_SetElectroPower(HANDLE hHandle, BOOL fOn);

#endif

